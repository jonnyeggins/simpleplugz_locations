<?php
namespace Simpleplugz\Locations\Block\Adminhtml\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

/**
 * Form Class
 */
class Form extends Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $_resource;
    protected $locate;
    protected $assetRepo;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param \Magento\Store\Model\System\Store       $systemStore
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Framework\App\ResourceConnection $resource,
        \Simpleplugz\Locations\Model\Locate $locate,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_resource = $resource;
        $this->locate = $locate;
        $this->assetRepo = $context->getAssetRepository();
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('form');
        $this->setTitle(__('Store Locations Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /**
         * @var \Simpleplugz\Locations\Model\Department $model
        */
        $model = $this->_coreRegistry->registry('locations_index');

        /**
         * @var \Magento\Framework\Data\Form $form
        */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post','enctype' => 'multipart/form-data']]
        );

        $fieldset = $form->addFieldset(
            'base_fieldset_info',
            ['legend' => __('Location Information'), 'class' => 'fieldset-wide locations']
        );

        if ($model->getId()) {
            $fieldset->addField('location_id', 'hidden', ['name' => 'location_id']);
        }

        $fieldset->addField(
            'location_name',
            'text',
            ['name' => 'location_name', 'label' => __('Location Name'), 'title' => __('Location Name'), 'required' => true]
        );

        //location address
        $ad = $fieldset->addField(
            'location_address',
            'text',
            ['name' => 'location_address', 'label' => __('Location Address'), 'title' => __('Location Address'), 'required' => true]
        );
        //get Success  image
        $successImg = $this->assetRepo->getUrl('Simpleplugz_Locations::images/tick.png');
        $errorImg = $this->assetRepo->getUrl('Simpleplugz_Locations::images/close-button.png');
        $ad->setAfterElementHtml('<div id="addressCorrect"><img src="'.$successImg.'" alt="Success"/></div><div id="addressError"><img src="'.$errorImg.'" alt="error"/></div>');
         //upload pointer image here
        $fieldset->addField(
            'custom_pointer',
            'image',
            ['name' => 'custom_pointer', 'label' => __('Custom Pointer'), 'title' => __('Custom Pointer'), 'required' => false
            ]
        );

        //location image
        $fieldset->addField(
            'image',
            'image',
            ['name' => 'image', 'label' => __('Location Image'), 'title' => __('Location Image'), 'required' => false
            ]
        );
        $fieldset = $form->addFieldset(
            'base_fieldset_page',
            ['legend' => __('Location Information Page'), 'class' => 'fieldset-wide locations']
        );

        //Geo location Lat
        $fieldset->addField(
            'geo_location_lat',
            'text',
            ['name' => 'geo_location_lat', 'label' => __('Lat'), 'title' => __('Geo Location Lat'), 'required' => true]
        );

        //Geo location Lng
        $lng =  $fieldset->addField(
            'geo_location_lng',
            'text',
            ['name' => 'geo_location_lng', 'label' => __('Lng'), 'title' => __('Geo Location Lng'), 'required' => true]
        );
        $fieldset->addField(
            'show_details_page',
            'radios',
            [
                'name' => 'show_details_page',
                'label' => __('Show Details Page'),
                'title' => __('show_details_page'),
                'required' => false,
                'values' => [
                                ['value' =>1, 'label' => __('Show Details Page')],
                                ['value' => 0, 'label' => __('Hide Details Page')]
                            ]
            ]
        );

        //store page url
        $fieldset->addField(
            'page_url',
            'text',
            ['name' => 'page_url', 'label' => __('Store URL'), 'title' => __('Store URL'), 'required' => false]
        );

        //phone
        $fieldset->addField(
            'phone',
            'text',
            ['name' => 'phone', 'label' => __('Phone'), 'title' => __('Store Phone'), 'required' => false]
        );

        //email
        $fieldset->addField(
            'email',
            'text',
            ['name' => 'email', 'label' => __('Email'), 'title' => __('Store Email'), 'required' => false]
        );
        //in your form the wysiwyg field will be like this
        $fieldset->addField(
            'details_texts',
            'editor',
            [
                'name' => 'details_texts',
                'label' => __('Content'),
                'title' => __('Content'),
                'style' => 'height:10em',
                'required' => false,
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        $fieldset = $form->addFieldset(
            'base_fieldset_times',
            ['legend' => __('Location Opening Hours'), 'class' => 'fieldset-wide locations']
        );

        $fieldset->addField(
            'show_closing_times',
            'radios',
            [
                'name' => 'show_closing_times',
                'label' => __('Closing Times'),
                'title' => __('show_closing_times'),
                'required' => false,
                'values' => [
                                ['value' =>1, 'label' => __('Show Closing Times')],
                                ['value' => 0, 'label' => __('Hide Closing Times')]
                            ]
            ]
        );

        //closing times
        $ct = $fieldset->addField(
            'closing_times',
            'text',
            ['name' => 'closing_times', 'label' => __(''), 'title' => __('Closing Times'), 'required' => false]
        );
        //closing times
        $ct = $fieldset->addField(
            'closing_times_special',
            'text',
            ['name' => 'closing_times_special', 'label' => __(''), 'title' => __('Closing Times '), 'required' => false]
        );

        $specialClosing = $this->locate->getSpecialClosingTimes($model->getId());
        $closingTimes = $this->locate->getEditClosingTimes($model->getId());

        if ($closingTimes->sunday->closed == '1') {
            $closingTimes->sunday->checked = ' checked="checked" ';
        } else {
            $closingTimes->sunday->checked = '';
        }
        if ($closingTimes->monday->closed == '1') {
            $closingTimes->monday->checked = ' checked="checked" ';
        } else {
            $closingTimes->monday->checked = '';
        }
        if ($closingTimes->tuesday->closed == '1') {
            $closingTimes->tuesday->checked = ' checked="checked" ';
        } else {
            $closingTimes->tuesday->checked = '';
        }
        if ($closingTimes->wednesday->closed == '1') {
            $closingTimes->wednesday->checked = ' checked="checked" ';
        } else {
            $closingTimes->wednesday->checked = '';
        }
        if ($closingTimes->thursday->closed == '1') {
            $closingTimes->thursday->checked = ' checked="checked" ';
        } else {
            $closingTimes->thursday->checked = '';
        }
        if ($closingTimes->friday->closed == '1') {
            $closingTimes->friday->checked = ' checked="checked" ';
        } else {
            $closingTimes->friday->checked = '';
        }
        if ($closingTimes->saturday->closed == '1') {
            $closingTimes->saturday->checked = ' checked="checked" ';
        } else {
            $closingTimes->saturday->checked = '';
        }

        $html = '<h3>Weekly Closing Times</h3><table class="table closingtimes weeklytimes">
    <tr>
    <th>Day</th>
    <th>Opening Time</th>
    <th>Closing Time</th>
    <th>Closed All Day</th>
  </tr>
  <tr data-day="0" class="weekday">
    <td><label style="font-weight:800">Sunday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->sunday->start_time, $closingTimes->sunday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->sunday->end_time, $closingTimes->sunday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->sunday->checked.' data-day="0" data-time="2" value="'.$closingTimes->sunday->closed.'" onclick="closedCheckboxClick(jQuery(this));" /> Closed</td>
  </tr>
  <tr data-day="1" class="weekday">
    <td><label style="font-weight:800">Monday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->monday->start_time, $closingTimes->monday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->monday->end_time, $closingTimes->monday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->monday->checked.'  data-day="1" data-time="2" value="'.$closingTimes->monday->closed.'" onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>
  </tr>
  <tr data-day="2" class="weekday">
    <td><label style="font-weight:800">Tuesday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->tuesday->start_time, $closingTimes->tuesday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->tuesday->end_time, $closingTimes->tuesday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->tuesday->checked.'  data-day="2" data-time="2" value="'.$closingTimes->tuesday->closed.'" onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>
  </tr>
  <tr data-day="3" class="weekday">
    <td><label style="font-weight:800">Wednesday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->wednesday->start_time, $closingTimes->wednesday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->wednesday->end_time, $closingTimes->wednesday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->wednesday->checked.'  data-day="3" data-time="2" value="'.$closingTimes->wednesday->closed.'" onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>
  </tr>
  <tr data-day="4" class="weekday">
    <td><label style="font-weight:800">Thursday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->thursday->start_time, $closingTimes->thursday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->thursday->end_time, $closingTimes->thursday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->thursday->checked.'  data-day="4" data-time="2" value="'.$closingTimes->thursday->closed.'" onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>
  </tr>
  <tr data-day="5" class="weekday">
    <td><label style="font-weight:800">Friday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->friday->start_time, $closingTimes->friday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->friday->end_time, $closingTimes->friday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->friday->checked.'  data-day="5" data-time="2" value="'.$closingTimes->friday->closed.'" onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>
  </tr>
  <tr data-day="6" class="weekday">
    <td><label style="font-weight:800">Saturday</label></td>
    <td><div class="storeopen">'.$this->renderTimes($closingTimes->saturday->start_time, $closingTimes->saturday->disabledTimes).'</div></td>
    <td><div class="storeclose">'.$this->renderTimes($closingTimes->saturday->end_time, $closingTimes->saturday->disabledTimes).'</div></td>
    <td><input type="checkbox" class="closing" '.$closingTimes->saturday->checked.'  data-day="6" data-time="2" value="'.$closingTimes->saturday->closed.'" onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>
  </tr>

</table>';
        $specialRows = [];

        foreach ($specialClosing as $closing) {
            $specialRows[] = $this->renderSpecialTimes($closing);
        }
        $specialRowsString = implode('', $specialRows);
        $html .= '<br><br><h3>Special Closing Times</h3><table class="table closingtimes specialtimes">
        <thead>
    <tr>
        <th>Date</th>
        <th>Day Name</th>
        <th>Opening Times</th>
        <th>Closing Times</th>
        <th>Closed All Day</th>
        <th></th>
    </tr></thead><tbody>'.$specialRowsString.'</tbody></table>';
         $html .= '<button class="btn btn-primary" style="float:right;margin-top:10px" onclick="addSpecialTimes(\''.$this->locate->getDateOrderJavascript().'\'); return false;">Add New</button>';

         $ct->setAfterElementHtml($html);

        $fieldset = $form->addFieldset(
            'base_fieldset_manager',
            ['legend' => __('Manager Information'), 'class' => 'fieldset-wide locations']
        );
        $fieldset->addField(
            'show_manager',
            'radios',
            [
                'name' => 'show_manager',
                'label' => __('Manager'),
                'title' => __('show_manager'),
                'required' => false,
                'values' => [
                                ['value' =>1, 'label' => __('Show Manager')],
                                ['value' => 0, 'label' => __('Hide Manager')]
                            ]
            ]
        );

        //manager name
        $fieldset->addField(
            'manager_name',
            'text',
            ['name' => 'manager_name', 'label' => __('Manager Name'), 'title' => __('Manager Name'), 'required' => false]
        );

        //manager email
        $fieldset->addField(
            'manager_email',
            'text',
            ['name' => 'manager_email', 'label' => __('Manager Email'), 'title' => __('Manager Email'), 'required' => false]
        );

        //manager image
        $fieldset->addField(
            'manager_image',
            'image',
            ['name' => 'manager_image', 'label' => __('Manager Image'), 'title' => __('Manager Image'), 'required' => false
            ]
        );

        //manager Phone
        $fieldset->addField(
            'manager_phone',
            'text',
            ['name' => 'manager_phone', 'label' => __('Manager Phone'), 'title' => __('Manager Phone'), 'required' => false]
        );

        $fieldset = $form->addFieldset(
            'base_fieldset_settings',
            ['legend' => __('Settings'), 'class' => 'fieldset-wide locations']
        );
        $fieldset->addField(
            'active',
            'radios',
            [
                'name' => 'active',
                'label' => __('Active'),
                'title' => __('Active'),
                'required' => true,
                'values' => [
                                ['value' =>1, 'label' => __('Enabled')],
                                ['value' => 0, 'label' => __('Disabled')]
                            ]
            ]
        );

        $defaultOptions = false;
        if ($defaultOptions) {
            $df = $fieldset->addField(
                'default',
                'radios',
                [
                    'name' => 'default',
                    'label' => __('Default Position'),
                    'title' => __('Default'),
                    'required' => true,
                    'values' => [
                                    ['value' =>0, 'label' => __('Not Set')],
                                    ['value' =>1, 'label' => __('Position 1')],
                                    ['value' =>2, 'label' => __('Position 2')],
                                    ['value' =>3, 'label' => __('Position 3')],
                                    ['value' => 4, 'label' => __('Position 4')]
                                ]
                ]
            );
            $df->setAfterElementHtml('<br><p>Show store locations before they enter their location details.</p>');
        }
        if (!$model->getId()) {
            $model->setData('active', '1');
        }
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function renderSpecialTimes($closing)
    {
        if ($closing['closed'] == 1) {
            $checked = 'checked="checked"';
            $disabledTimes = ' disabled="disabled" ';
        } else {
            $checked = '';
            $disabledTimes = '';
        }
        $closing['special_date'] = $this->locate->formatDateForDatabaseReverse($closing['special_date']);

        $html = '<tr data-specialid="'.$closing['id'].'">';

        $html .= '<td><input type="text" class="input-text admin__control-text datepicker"  value="'.$closing['special_date'].'"/></td>';
        $html .= '<td><input type="text" class="input-text admin__control-text dayname" value="'.$closing['day'].'"/></td>';

        if (isset($closing['start_time']) && $closing['start_time'] != '') {
            //startime
            $html .= '<td  class="storeopen">'.$this->renderTimes($closing['start_time'], $disabledTimes).'</td>';
        } else {
            $html.= '<td></td>';
        }

        if (isset($closing['end_time']) && $closing['end_time'] != '') {
            //endtime
            $html .= '<td  class="storeclose">'.$this->renderTimes($closing['end_time'], $disabledTimes).'</td>';
        } else {
            $html.= '<td></td>';
        }
        //closing
        $html .= '<td><input type="checkbox" class="closing"  data-day="0" data-time="2" '.$checked.' onclick="closedCheckboxClick(jQuery(this));"/> Closed</td>';
        $html .= '<td><a href="#" onclick="removeClosingTimes(jQuery(this))">Remove</a></td>';
        return $html;
    }

    protected function AddNewSpecialTimes()
    {
        $empty = [];
        $empty['start_time'] = '09:00';
        $empty['end_time'] = '17:00';
        $empty['closed'] = '0';
        $empty['disabledTimes'] = '';
        $empty['day'] = '';
        $empty['special_date'] = '';
        $empty['id'] = '';
        return $this->renderSpecialTimes($empty);
    }

    protected function renderTimes($selected, $disabledTimes)
    {

        if (strstr($selected, ':')) {
            $selected = date("g:i a", strtotime($selected));
            list($h,$m) = explode(':', $selected);
            list($rubbish,$ampm) = explode(' ', $selected);
        } else {
            $h = '';
            $m = '';
            $ampm = '';
        }
        $hoursReturned = '<select class="renderTimesHours select admin__control-select" '.$disabledTimes.'>
            <option data-hour="1">1</option>
            <option data-hour="2">2</option>
            <option data-hour="3">3</option>
            <option data-hour="4">4</option>
            <option data-hour="5">5</option>
            <option data-hour="6">6</option>
            <option data-hour="7">7</option>
            <option data-hour="8">8</option>
            <option data-hour="9">9</option>
            <option data-hour="10">10</option>
            <option data-hour="11">11</option>
            <option data-hour="12">12</option>
        </select>';

        $hoursReturned = str_replace('data-hour="'.$h.'"', 'data-hour="'.$h.'" selected', $hoursReturned);

        $minsReturned = '<select class="renderTimesMins select admin__control-select" '.$disabledTimes.'>
            <option data-min="00">00</option>
            <option data-min="05">05</option>
            <option data-min="10">10</option>
            <option data-min="15">15</option>
            <option data-min="20">20</option>
            <option data-min="25">25</option>
            <option data-min="30">30</option>
            <option data-min="35">35</option>
            <option data-min="40">40</option>
            <option data-min="45">45</option>
            <option data-min="50">50</option>
            <option data-min="55">55</option>
        </select>';

        $minsReturned = str_replace('data-min="'.$m.'"', 'data-min="'.$m.'" selected', $minsReturned);
         $ampmText = '<select class="renderTimesText select admin__control-select" '.$disabledTimes.'>';

        //sroting out morning
        if (strtolower($ampm) == 'am') {
            $ampmText .= '<option value="AM" selected="selected">AM</option>';
        } else {
            $ampmText .= '<option value="AM">AM</option>';
        }

        if (strtolower($ampm) == 'pm') {
            $ampmText .= '<option value="PM" selected="selected">PM</option>';
        } else {
            $ampmText .= '<option value="PM">PM</option>';
        }

        $ampmText .= '</select>';
        $ampmText = str_replace('value="'.$ampm.'"', 'value="'.$ampm.'" selected', $ampmText);

        return $hoursReturned.$minsReturned.$ampmText;
    }
}
