<?php
namespace Simpleplugz\Locations\Block\Adminhtml;

use Magento\Backend\Block\Widget\Form\Container;

/**
 * Editing a Store Location using a Form
 */
class Edit extends Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Locations edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'Simpleplugz_Locations';
        $this->_controller = 'adminhtml';

        parent::_construct();

        if ($this->_isAllowedAction('Simpleplugz_Locations::save')) {
            $this->buttonList->update('save', 'label', __('Save Location'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }
    }

    /**
     * Get header with Locations name
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->coreRegistry->registry('locations_index')->getId()) {
            $LocationName =  $this->coreRegistry->registry('locations_index')->getData('location_name');
            return __("Edit Locations '%1'", $this->escapeHtml($LocationName));
        } else {
            return __('New Locations');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param  string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        $options = ['_current' => true, 'back' => 'edit', 'active_tab' => ''];
        return $this->getUrl('simpleplugz_locations/*/save', $options);
    }
}
