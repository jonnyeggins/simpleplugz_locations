<?php
namespace Simpleplugz\Locations\Block\Adminhtml;

class ListExtra extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_grid';
        $this->_blockGroup = 'Simpleplugz_Locations';
        $this->_addButtonLabel = __('Add New Location');
        parent::_construct();
        $this->buttonList->add(
            'locations_config',
            [
                'label' => __('Config'),
                'onclick' => "location.href='" . $this->getUrl('adminhtml/system_config/edit/section/store_locations') . "'",
                'class' => 'apply'
            ]
        );
        $this->buttonList->add(
            'locations_import',
            [
                'label' => __('Import CSV Locations/Opening Times'),
                'onclick' => "location.href='" . $this->getUrl('adminhtml/import/index') . "'",
                'class' => 'apply'
            ]
        );
    }
}
