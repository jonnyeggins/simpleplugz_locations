<?php
namespace Simpleplugz\Locations\Block\Adminhtml;

/**
 * Get Form
 */
class Main extends \Magento\Backend\Block\Template
{
    protected $locate_helper;
    public function __contruct()
    {
        parent::__contruct();
    }
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Simpleplugz\Locations\Helper\Data $locate_helper,
        array $data = []
    ) {
        $this->locate_helper = $locate_helper;
        parent::__construct($context, $data);
    }
    //
    /**
     * get the google api key
     *
     * @return string Google Api Key
     */
    public function getApiKey()
    {
        return $this->locate_helper->getConfig('store_locations/store_locations_group/google_api_code');
    }

    /**
     * get order to display the date javascript from magento config
     *
     * @return string date order in javascript format
     */
    public function getDateOrderJavascript()
    {
        $order = $this->locate_helper->getConfig('catalog/custom_options/date_fields_order');
        if ($order == 'd,m,y') {
            return 'dd/mm/yy';
        } elseif ($order == 'm,d,y') {
            return 'mm/dd/yy';
        } else {
            return 'yy/mm/dd';
        }
    }
}
