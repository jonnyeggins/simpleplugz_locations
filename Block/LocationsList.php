<?php
namespace Simpleplugz\Locations\Block;

use Simpleplugz\Locations\Model\Locate;

class LocationsList extends \Magento\Framework\View\Element\Template
{
    protected $helperData;
    protected $assetRepo;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Simpleplugz\Locations\Model\LocationsItemFactory $locateFactory,
        \Simpleplugz\Locations\Model\Locate $locate,
        \Simpleplugz\Locations\Helper\Data $helperData
    ) {
        $this->locateFactory = $locateFactory;
        $this->locate = $locate;
        $this->helperData = $helperData;
        $this->assetRepo = $context->getAssetRepository();
        parent::__construct($context);
    }

    /**
     * Get Locations based off geo posted data
     *
     * @return array locations
     */
    public function getLocations()
    {
        $post = $this->getRequest()->getPostValue();
        if (isset($post['lat'])) {
            $geo = [];
            $geo['lat'] = $post['lat'];
            $geo['lng'] = $post['lng'];
            $locations = $this->locate->getLocationsFromGeo($geo);
            return $locations;
        } else {
             $locations = $this->locate->getDefaultStartLocations();
              return $locations;
        }
    }

    /**
     * Get the users locations of via the psoted data
     * @return [type] [description]
     */
    public function getUsersLocationPost()
    {
        $post = $this->getRequest()->getPostValue();
        if (isset($post['lat'])) {
            $geo = [];
            $geo['lat'] = $post['lat'];
            $geo['lng'] = $post['lng'];
            return $geo;
        } else {
            return [];
        }
    }

    /**
     * Get the search Radius distance from the config
     *
     * @return int search radius distance
     */
    public function getSearchRadius()
    {
        $search_radius =  $this->helperData->getConfig('store_locations/store_locations_group/search_radius');
        if ($search_radius != '' && !is_numeric($search_radius)) {
            return 50; // default number as backup
        }
        return $search_radius;
    }

    /**
     * Get the distance unit
     *
     * @return string KM/Miles
     */
    public function getDistUnit()
    {
        $type = $this->helperData->getConfig('store_locations/store_locations_group/distance_type');
        if ($type == 'kms') {
            return 'kms';
        } else {
            return 'miles';
        }
    }

    public function getIcons()
    {
        $storeicon = $this->assetRepo->getUrl('Simpleplugz_Locations::images/store.png');
        $directionsicon = $this->assetRepo->getUrl('Simpleplugz_Locations::images/directions.png');
        return ['storeIcon'=>$storeicon,'directionsIcon'=>$directionsicon];
    }
}
