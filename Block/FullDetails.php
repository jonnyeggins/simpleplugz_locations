<?php
namespace Simpleplugz\Locations\Block;

use Simpleplugz\Locations\Model\Locate;

class FullDetails extends \Magento\Framework\View\Element\Template
{
    public $locate_helper;
    public $locateFactory;
    public $escaper;
    public $storeDetails;
    public $settings;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Simpleplugz\Locations\Model\LocationsItemFactory $locateFactory,
        \Simpleplugz\Locations\Model\Locate $locate,
        \Simpleplugz\Locations\Helper\Data $locate_helper
    ) {
        $this->locateFactory = $locateFactory;
        $this->locate = $locate;
        $this->locate_helper = $locate_helper;
        $this->escaper = $context->getEscaper();
        parent::__construct($context);
        //request settings straight away
        $this->getSettings();
    }

    //get the api from the database
    public function getApiKey()
    {
        return $this->locate_helper->getConfig('store_locations/store_locations_group/google_api_code');
    }

    //get the full store details
    public function getStoreDetails()
    {
        $storeid = $this->getRequest()->getParam('storeid');
        if ($storeid == '') {
            $storeUrl = $this->getRequest()->getParam('store');
            $storeid = $this->locate->getStoreIdFromUrl($storeUrl);
        }

        if (!is_numeric($storeid)) {
             $this->_redirect('locations');
        }
        $location = $this->locateFactory->create();
        $this->storeDetails = $location->load($storeid);
        $html = $this->getHtmlDetails($this->storeDetails);
        $this->storeDetails->setDetailsTexts($html);
    }

    //get all the settings from the locate model
    public function getSettings()
    {
        $this->settings =  $this->locate->getSettings();
    }

    //get the closing times information
    public function getClosingTimes()
    {
        $storeid = $this->getRequest()->getParam('storeid');
        $calculateSpecialDates = true;
        return $this->locate->getClosingTimes($storeid, $calculateSpecialDates, false);
    }

    //get the closing times starting array
    public function getClosingTimesStartArray($times)
    {
        return $this->locate->getClosingTimesStartArray($times);
    }

    /**
     * limit what html tags are allowed for security reasons
     *
     * @param  obj $storeDetails store details object
     * @return html safe html
     */
    public function getHtmlDetails($storeDetails)
    {
        $data = $storeDetails->getData('details_texts');
        return $this->escaper->escapeHtml($data, $this->getAllowedTags());
    }

    public function _prepareLayout()
    {
        $this->getStoreDetails();
       //set page title
        $this->pageConfig->getTitle()->set($this->storeDetails->getLocationName(). ' - '.__('Store Details'));

        return parent::_prepareLayout();
    }

    /**
     * tags that are allowed to be displayed (obvious 'script' not in the list for security reasons)
     *
     * @return array allowed tags
     */
    public function getAllowedTags()
    {
        return ['div','p','h1','h2','h3','h4','h5','h6','ul','ol','li','dl','dt','dd','address','hr','pre','blockquote','center','ins','del','a','span','bdo','br','em','strong','dfn','code','samp','kbd','bar','cite','abbr','acronym','q','sub','sup','tt','i','b','big','small','u','s','strike','basefont','font','object','param','img','table','caption','colgroup','col','thead','tfoot','tbody','tr','th','td','embed','iframe','video','audio'];
    }
}
