<?php
namespace Simpleplugz\Locations\Block;

use Magento\Framework\View\Element\Template;

/**
 * Get Block details for map
 */
class Map extends Template
{
    private $countryFactory;
    private $locate;
    private $locate_helper;
    protected $assetRepo;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Simpleplugz\Locations\Model\Locate $locate,
        \Simpleplugz\Locations\Helper\Data $locate_helper
    ) {
            $this->countryFactory = $countryFactory;
            $this->locate = $locate;
            $this->locate_helper = $locate_helper;
            $this->assetRepo = $context->getAssetRepository();
            parent::__construct($context);
    }


    /**
     * Hide address bar if no api key
     *
     * @return bool false hide API Key
     */
    public function showAddressSearchBar()
    {
        if ($this->getApiKey() == '') {
            return false;
        } else {
            return true;
        }
    }

    //not used?
    /**
     * get the locations that should come up if no user address is given yet
     *
     * @return array default locations
     */
    public function getDefaultLocations()
    {
        $l = $this->locate;
        $locations = $l->getDefaultStartingLocation();
        return $locations;
    }

    /**
     * get the goolge api key from the database for the map api
     *
     * @return string Google API
     */
    public function getApiKey()
    {
        return $this->escapeHtml($this->locate_helper->getConfig('store_locations/store_locations_group/google_api_code'));
    }

    /**
     * get the text to display on the first tab
     *
     * @return string zipcode/suburb output
     */
    public function getPostcodeTabText()
    {
        $return = $this->escapeHtml($this->locate_helper->getConfig('store_locations/store_locations_group/postcode_or_zip'));
        if ($return  == '') {
            return 'Zip Code/Suburb';
        } else {
            return $return;
        }
    }

    public function _prepareLayout()
    {
       //set page title
        $this->pageConfig->getTitle()->set(__('Store Finder'));
        return parent::_prepareLayout();
    }

    /**
     * get what country (if any) the user has limited the addresses to.
     *
     * @return string country name
     */
    public function getCountry()
    {
        $countryCode =  $this->locate_helper->getConfig('store_locations/store_locations_group/countries');
        if ($countryCode != '') {
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            return $country->getName();
        } else {
            return '';
        }
    }

    public function getMapLoaderImage()
    {
        return $this->assetRepo->getUrl('Simpleplugz_Locations::images/maploader-min.png');
    }

    public function splitMobile()
    {
        $mobileTemplate = $this->locate_helper->getConfig('store_locations/store_locations_group/splitmobile');
        if ($mobileTemplate == 'split') {
            return true;
        } else {
            return false;
        }
    }

    public function mobileClass()
    {
        $splitmobile = $this->locate_helper->getConfig('store_locations/store_locations_group/splitmobile');
        return $splitmobile;
    }
}
