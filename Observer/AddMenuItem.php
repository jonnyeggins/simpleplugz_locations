<?php
namespace Simpleplugz\Locations\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Data\Tree\Node;

class AddMenuItem implements ObserverInterface
{
    public $locate_helper;
    public $store_helper;
    public function __construct(
        \Simpleplugz\Locations\Helper\Data $locate_helper,
        \Magento\Framework\UrlInterface $store_helper
    ) {
    
        $this->locate_helper = $locate_helper;
        $this->store_helper = $store_helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        //check if we should add a menu item to the main menu
        if ($this->locate_helper->getConfig('store_locations/store_locations_group/show_in_menu') != 1) {
            //dont add in the menu
        } else {
            //add to menu
            $menu = $observer->getData('menu');
            // @codingStandardsIgnoreEnd
            $node = new Node(
                $this->getNodeAsArray(),
                'id',
                $menu->getTree(),
                $menu
            );
            $menu->addChild($node);
        }
    }

  /**
   * @return array
   */
    protected function getNodeAsArray()
    {
        $is_active = false;
        $currentUrl = $this->store_helper->getCurrentUrl();
        $getBaseUrl = $this->store_helper->getUrl();
        $path = str_replace($getBaseUrl, '', $currentUrl);
        if ($path == 'locations') {
            $is_active = true;
        }
        return [
            'name' => __('Store Finder'),
            'id' => 'store-finder',
            'url' => '/locations',
            'has_active' => false,
            'is_active' => $is_active
        ];
    }
}
