---------------------------------------------

Simpleplugz Locations Release Notes

---------------------------------------------
Version 1.0.7
---------------------------------------------
Feature Added
- Place card on single location
Bug Fixes
- Fixed Install demo Images

---------------------------------------------
Version 1.0.6
---------------------------------------------
Bug Fixes
- Removed space from acl XML

---------------------------------------------
Version 1.0.5
---------------------------------------------
Bug Fixes
- Fixed Layout Issues for mobile

---------------------------------------------
Version 1.0.4
---------------------------------------------
Changes
- Updated template with better responsive layout

---------------------------------------------
Version 1.0.3
---------------------------------------------
Changes
- Fixed javascript loop

---------------------------------------------
Version 1.0.2
---------------------------------------------
Changes
- Font Color Changes
- Fixed Issue for Magento 2.0.14 There was an issue causing white screen on install
- Fixed Issue where locations not showing by default
- Added Title on Page
- Added 'alt' text to images

---------------------------------------------
Version 1.0.1
---------------------------------------------
- Initial Release
