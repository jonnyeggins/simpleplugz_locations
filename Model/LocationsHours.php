<?php
namespace Simpleplugz\Locations\Model;

use Magento\Framework\Model\AbstractModel;

class LocationsHours extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
            $this->_init('Simpleplugz\Locations\Model\ResourceModel\LocationsHours');
    }
}
