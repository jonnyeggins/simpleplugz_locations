<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Simpleplugz\Locations\Model\Source\Import;

/**
 * Import behavior source model used for defining the behaviour during the import.
 */
class Behavior extends \Magento\ImportExport\Model\Source\Import\AbstractBehavior
{
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            \Magento\ImportExport\Model\Import::BEHAVIOR_APPEND => __('Add/Update'),
            \Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE => __('Replace')
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return 'store_locations';
    }
}
