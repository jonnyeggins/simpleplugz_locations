<?php
namespace Simpleplugz\Locations\Model;

class LocationsItem extends \Magento\Framework\Model\AbstractModel implements LocationsItemInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'simpleplugz_locations';

    protected function _construct()
    {
        $this->_init('Simpleplugz\Locations\Model\ResourceModel\LocationsItem');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
