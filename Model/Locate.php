<?php
namespace Simpleplugz\Locations\Model;

/**
 * Locate Class gets store locations infomation from database and formats
 * into usable infomation, also contains helper business logic
 */
class Locate
{

    private $connection;
    private $_resource;
    private $locateFactory;
    private $hoursFactory;
    private $formData;
    private $helperdata;
    private $stdClass;
    private $messageManager;
    private $logger;
    private $_objectManager;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Simpleplugz\Locations\Model\LocationsItemFactory $locateFactory,
        \Simpleplugz\Locations\Model\LocationsHoursFactory $hoursFactory,
        \Simpleplugz\Locations\Helper\Data $helperdata,
        \Simpleplugz\Locations\Model\SimpleplugzStdClass $stdClass,
        \Magento\Framework\App\Action\Context $context,
        \Psr\Log\LoggerInterface $logger
    ) {
          $this->connection = $resource->getConnection();
          $this->locateFactory = $locateFactory;
          $this->hoursFactory = $hoursFactory;
          $this->helperdata = $helperdata;
          $this->stdClass = $stdClass;
          $this->logger = $logger;
          $this->messageManager = $context->getMessageManager();
          $this->_objectManager = $context->getObjectManager();
    }

    /**
     * Get the database connection
     *
     * @return obj Return Database object
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Get config settings from the database
     *
     * @return json config settings
     */
    public function getSettingsFromDatabase()
    {
        $settings = $this->getSettings();
        return $settings;
    }

    /**
     * Show error message
     *
     * @param  string $message Error message
     * @return null
     */
    public function showErrorMessage($message)
    {
        $this->_messageManager->addError($message);
    }

    public function errorJson($message)
    {
        $message = $this->stdClass->create();
        $message->unknown = true;
        $message->message  = $message;
        return $message;
    }

    /**
     * get all the details for the store including the settings
     *
     * @param  int $locationId Id of the store location
     * @return json store details
     */
    public function getStoreDetailsSettings($storeid)
    {
        $storeSettings = $this->stdClass->create();
        //get main settings
        $settings = $this->getSettings();
        $storeSettings->icon = $settings->icon;
        $storeSettings->usericon = $settings->usericon;
        $collection = $this->locateFactory->create()->getCollection();
        $collection->addFieldToSelect('location_name');
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToSelect('geo_location_lat');
        $collection->addFieldToSelect('geo_location_lng');
        $collection->addFieldToSelect('custom_pointer');
        $collection->addFieldToFilter('location_id', ['eq' => $storeid]);
        $results = $collection->load();

        if (!$results->getData() || empty($results->getData())) {
            return $this->errorJson("Sorry Map Offline");
        } else {
            $store = null;
            foreach ($results as $r) {
                $store = $r->getData();
            }
        }

            //set the store settings
            $storeSettings->storename = $store['location_name'];
            $storeSettings->geo_location_lat = $store['geo_location_lat'];
            $storeSettings->geo_location_lng = $store['geo_location_lng'];
        if ($store['custom_pointer'] != '') {
            $storeSettings->icon = '/pub/media/'.$store['custom_pointer'];
        }
        return $storeSettings;
    }

    /**
     * Get the country/countries that are selected in the locations config settings
     *
     * @return array countrycodes
     */
    public function getCountry()
    {
        return $this->helperdata->getConfig('store_locations/store_locations_group/countries');
    }

    /**
     * get object settings from config for locations map
     *
     * @return obj $settings Return settings object
     */
    public function getSettings()
    {

        $settings = $this->stdClass->create();
         $settings->loadAll = true;
        // //load all markers

        //allow geo detect
        if ($this->helperdata->getConfig('store_locations/store_locations_group/allow_geo_detect') == '1') {
            $settings->detectUser = true;
        } else {
            $settings->detectUser = false;
        }

          //detect countries
          $settings->countryDetect = strtolower($this->helperdata->getConfig('store_locations/store_locations_group/countries'));

          //default store locations
          $defaultLoc = $this->getDefaultStartingLocation();
          $settings->defaultLocation = $this->stdClass->create();
        if (isset($defaultLoc[0])) {
            $settings->defaultLocation->lat = $defaultLoc[0]['geo_location_lat'];
            $settings->defaultLocation->lng = $defaultLoc[0]['geo_location_lng'];
        }

          //icons
           $settings->icon ='/pub/media/stores/'.$this->helperdata->getConfig('store_locations/store_locations_group/default_icon');
          $settings->usericon ='/pub/media/stores/'.$this->helperdata->getConfig('store_locations/store_locations_group/user_icon');

          $settings->splitmobile = strtolower($this->helperdata->getConfig('store_locations/store_locations_group/splitmobile'));
          //non customer settings
          $settings->mobileview = 'list'; //always tart withthe list on mobiles
          $settings->ipDetect = false;//ip detect is not setup yet

          return $settings;
    }

    /**
     * Convert Miles to Kilometers
     *
     * @param  int $miles Miles to be converted
     * @return int Kilometers
     */
    public function convertMilesToKM($miles)
    {
        return $miles * 1.609344;
    }

    
 /**
  * Get the default locations
  *
  * @return array Returns array of stores infomation
  */
    public function getDefaultStartingLocation()
    {
        $collection = $this->locateFactory->create()->getCollection();
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToSelect('geo_location_lat');
        $collection->addFieldToSelect('geo_location_lng');
        $collection->addFieldToFilter('default', ['neq' =>0]);
        $collection->addFieldToFilter('active', ['eq' =>1]);
        $collection->setPageSize(1);
        $collection->setCurPage(1);
        $collection->setOrder('`default`');
        $results = $collection->load();

        //could not find any default otions
        if (empty($results)) {
            $extraCollection = $this->locateFactory->create()->getCollection();
            $extraCollection->addFieldToSelect('location_id');
            $extraCollection->addFieldToSelect('geo_location_lat');
            $extraCollection->addFieldToSelect('geo_location_lng');
            $extraCollection->addFieldToFilter('default', ['eq' =>0]);
            $extraCollection->addFieldToFilter('active', ['eq' =>1]);
            $extraCollection->setPageSize(1);
            // $extraCollection->setPageSize(10);
            $extraCollection->setCurPage(1);
            $extraCollection->setOrder('location_id', 'DESC');
            $results = $extraCollection->load();
            // foreach ($extraResults as $extra){
            //   $results[] = $extra;
            // }
        }

        $return = [];
        foreach ($results as $result) {
            $re = [];
            $re['geo_location_lat'] = $result->getGeoLocationLat();
            $re['geo_location_lng'] = $result->getGeoLocationLng();
            $return[] = $re;
        }
        return $return;
    }

    /**
     * Get the default locations
     *
     * @return array Returns array of stores infomation
     */
    public function getDefaultStartLocations()
    {
        try {
            $combinedResults = [];
            $collection = $this->locateFactory->create()->getCollection();
            $collection->addFieldToSelect('location_id', 'id');
            $collection->addFieldToSelect('location_name');
            $collection->addFieldToSelect('location_address');
            $collection->addFieldToSelect('geo_location_lat');
            $collection->addFieldToSelect('geo_location_lng');
            $collection->addFieldToSelect('custom_pointer');
            $collection->addFieldToSelect('details_texts');
            $collection->addFieldToSelect('show_details_page');
            $collection->addFieldToSelect('page_url');
            $collection->addFieldToSelect('phone');
            $collection->addFieldToSelect('image');
            $collection->addFieldToSelect('active');
            $collection->addFieldToFilter('active', ['eq' => 1]);
            $collection->addFieldToFilter('default', ['gteq'=>1]);
            $collection->setPageSize(10);
            $collection->setCurPage(1);
            $collection->setOrder('`default`', 'ASC');
            $results = $collection->load();
            $numDefaults = 0; // country how many default results hwere found
            foreach ($results as $dr) {
                $numDefaults++;
                $combinedResults[] = $dr;
            }

            //check to see if it has selected at least 4 stores
            if ($numDefaults < 4) {
                $collection = $this->locateFactory->create()->getCollection();
                $collection->addFieldToSelect('location_id', 'id');
                $collection->addFieldToSelect('location_name');
                $collection->addFieldToSelect('location_address');
                $collection->addFieldToSelect('geo_location_lat');
                $collection->addFieldToSelect('geo_location_lng');
                $collection->addFieldToSelect('custom_pointer');
                $collection->addFieldToSelect('details_texts');
                $collection->addFieldToSelect('show_details_page');
                $collection->addFieldToSelect('page_url');
                $collection->addFieldToSelect('phone');
                $collection->addFieldToSelect('image');
                $collection->addFieldToSelect('active');
                $collection->addFieldToFilter('active', ['eq' => 1]);
                $collection->addFieldToFilter('default', ['eq' =>0]);
                $limitNumber = 4 - $numDefaults;
                $collection->setPageSize($limitNumber);
                $collection->setCurPage(1);
                $collection->setOrder('location_id', 'DESC'); // display the latest stores first
                $extraResults = $collection->load();
                foreach ($extraResults as $r) {
                    $combinedResults[] = $r;
                }
            }

            $combinedResults = $this->formatLocations($combinedResults, true);

            return  $combinedResults;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            return $this->stdClass->create(); // return a blank object
        }
    }

    public function getStoreIdFromUrl($url)
    {
        $collection = $this->locateFactory->create()->getCollection();
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToFilter('page_url', ['eq' =>$url]);
        $results = $collection->load();
        foreach ($results as $result) {
            return $result->getLocationId();
        }
        return null;
    }

    /**
     * Get Locations that are closest to that users Geo Co-Ords
     *
     * @param  array $geoLocation array of
     * @return array store details
     */
    public function getLocationsFromGeo($geoLocation)
    {
        $search_radius =  $this->helperdata->getConfig('store_locations/store_locations_group/search_radius');
        $geoLocation['lng'] = (double) $geoLocation['lng'];
        $geoLocation['lat'] = (double) $geoLocation['lat'];
        try {
             $query = "SELECT location_id as id ,
         ( 3959 * acos( cos( radians(:lat) ) * cos( radians( geo_location_lat ) ) * cos( radians( geo_location_lng ) - radians(:lng) ) + sin( radians(:lat) ) * sin( radians( geo_location_lat ) ) ) ) AS distance,
         location_name,
         location_address,
         geo_location_lat,
         geo_location_lng,
         custom_pointer,
         details_texts,
         show_details_page,
         page_url,
         phone,
         image,
         active

         FROM ".$this->connection->getTableName('simpleplugz_locations')." 
         Where active = '1' ";
            if ($search_radius != '') {
                 $query .= " HAVING distance < :distance ";
            }
            $query .= " ORDER BY distance LIMIT 0 , 10 ";

            if ($search_radius != '') {
                $binds = [
                ':lng' => $geoLocation['lng'],
                ':lat' => $geoLocation['lat'],
                ':distance' => $distance
                 ];
            } else {
                $binds = [
                ':lng' => $geoLocation['lng'],
                ':lat' => $geoLocation['lat']
                ];
            }
                $results = $this->connection->fetchAll($query, $binds);
                //not results found
            if (empty($results)) {
                $binds[':distance'] = 5000;
                $results = $this->connection->fetchAll($query, $binds);
            }
                $results = $this->formatLocations($results, false);
                return $results;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
             $this->logger->critical($e->getMessage());
             return $this->stdClass->create();
        }
    }

    /**
     * Get all the locations from the database (for pinning all locations on map)
     *
     * @return json
     */
    public function getAllLocations()
    {
          $list = $this->locateFactory->create();

          $collection = $list->getCollection();
          $collection->addFieldToSelect('location_id');
          $collection->addFieldToSelect('location_name');
          $collection->addFieldToSelect('location_address');
          $collection->addFieldToSelect('geo_location_lat');
          $collection->addFieldToSelect('geo_location_lng');
          $collection->addFieldToSelect('show_details_page');
          $collection->addFieldToSelect('page_url');
          $collection->addFieldToSelect('image');
          $collection->addFieldToSelect('custom_pointer');
          $collection->addFieldToFilter('active', ['eq'=>'1']);

          $locations = [];

        foreach ($collection as $col) {
            $location = $this->stdClass->create();
            $location->id = $col ->getData('location_id');
            $location->location_name = $col->getData('location_name');
            $location->location_address = $col->getData('location_address');
            $location->geo_location_lat = $col->getData('geo_location_lat');
            $location->geo_location_lng = $col->getData('geo_location_lng');
            $location->show_details_page = $col->getData('show_details_page');
            $location->page_url = $col->getData('page_url');
            $location->image = $col ->getData('image');
            $location->custom_pointer = $col->getData('custom_pointer');
            $location->url = '';
            $location->fulldetails = '/'.$location->page_url;

            if ($location->image != '') {
                $location->image = '/pub/media/'.$location->image;
            }
            if ($location->custom_pointer != '') {
                $location->custom_pointer = '/pub/media/'.$location->custom_pointer;
            } else {
                $location->custom_pointer = '/pub/media/stores/'.$this->helperdata->getConfig('store_locations/store_locations_group/default_icon');
            }
            $locations[] = $location;
        }
        return $locations;
    }

    /**
     * Format the location infomation
     *
     * @param  array $results Location information
     * @return array formatted information
     */
    public function formatLocations($results, $dbObj)
    {
        $miles = false;
        //need to add settings

        $formattedResults = [];

        foreach ($results as $result) {
            if ($dbObj) {
                $res = $result->getData();
            } else {
                $res = $result;
            }
            //check if it is json

            if (isset($res['distance'])) {
                if ($miles != true) {
                    $res['distance'] = $this->convertMilesToKM($res['distance']);
                }
                if ($res['distance'] < 1) {
                    $res['distance'] = '< 1';
                }
                $res['distance'] = number_format((float)$res['distance'], 1, '.', '');
                if ($miles != true) {
                    $res['distance'] = $res['distance'].'km';
                } else {
                    $res['distance'] = $res['distance'].'Miles';
                }
            }
            if (!isset($res['id'])) {
                $res['id'] = $res['location_id'];
            }

            if (isset($res['image']) && $res['image'] != '') {
                $res['image'] = '/pub/media/'.$res['image'];
            }
            if (isset($res['custom_pointer']) && $res['custom_pointer'] != '') {
                $res['custom_pointer'] = '/pub/media/'.$res['custom_pointer'];
            } else {
                $res['custom_pointer'] = '/pub/media/stores/'.$this->helperdata->getConfig('store_locations/store_locations_group/default_icon');
            }

              $res['fulldetails'] = '/'.$res['page_url'];

              $formattedResults[] = $res;
        }
        return $formattedResults;
    }

    /**
     * Import a new store location
     *
     * @param  array $data Location Information
     * @return null
     */
    public function importLocation($data)
    {
        if (!isset($data['default'])) {
            $data['default'] = 0;
        }
      //add stores image directory if not blank
        if ($data['custom_pointer'] != '') {
            $data['custom_pointer'] = 'stores/'.$data['custom_pointer'];
        }
        if ($data['image'] != '') {
            $data['image'] = 'stores/'.$data['image'];
        }
        if ($data['manager_image'] != '') {
            $data['manager_image'] = 'stores/'.$data['manager_image'];
        }
        try {
            $dataObj = [
            'location_name' => $data['location_name'],
            'location_address' => $data['location_address'],
            'geo_location_lat' => $data['geo_location_lat'],
            'geo_location_lng' => $data['geo_location_lng'],
            'custom_pointer' => $data['custom_pointer'],
            'details_texts' => $data['details_texts'],
            'show_details_page' => $data['show_details_page'],
            'show_closing_times' => $data['show_closing_times'],
            'show_manager' => $data['show_manager'],
            'page_url' => $data['page_url'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'image' => $data['image'],
            'manager_image' => $data['manager_image'],
            'manager_name' => $data['manager_name'],
            'manager_email' => $data['manager_email'],
            'manager_phone' => $data['manager_phone'],
            'active' => $data['active'],
            'default' => $data['default']

            ];

            $insertLocation = $this->locateFactory->create()->setData($dataObj);
            $insertLocation->save();

            return $insertLocation->getId();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            throw new \Exception('Error Inserting New Location');
        }
    }

    /**
     * Update the store location infomation
     *
     * @param  array Location Information
     * @return null
     */
    public function updateLocation($data)
    {
        //add stores image directory if not blank
        if ($data['custom_pointer'] != '') {
            $data['custom_pointer'] = 'stores/'.$data['custom_pointer'];
        }
        if ($data['image'] != '') {
            $data['image'] = 'stores/'.$data['image'];
        }
        if ($data['manager_image'] != '') {
            $data['manager_image'] = 'stores/'.$data['manager_image'];
        }
        if (!isset($data['default'])) {
            $data['default'] = '0';
        }
        try {
            $dataObj = [
            'location_id' => $data['location_id'],
            'location_name' => $data['location_name'],
            'location_address' => $data['location_address'],
            'geo_location_lat' => $data['geo_location_lat'],
            'geo_location_lng' => $data['geo_location_lng'],
            'custom_pointer' => $data['custom_pointer'],
            'details_texts' => $data['details_texts'],
            'show_details_page' => $data['show_details_page'],
            'show_closing_times' => $data['show_closing_times'],
            'show_manager' => $data['show_manager'],
            'page_url' => $data['page_url'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'image' => $data['image'],
            'manager_image' => $data['manager_image'],
            'manager_name' => $data['manager_name'],
            'manager_email' => $data['manager_email'],
            'manager_phone' => $data['manager_phone'],
            'active' => $data['active'],
            'default' => $data['default']

            ];

            $updateLocationDetails = $this->locateFactory->create()->load($data['location_id'])->setData($dataObj);
            $updateLocationDetails->save();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
             throw new \Exception('Error updating Location');
        }
    }

    /**
     * Get Closing Times starting with today
     *
     * @param  obj $times times object containing closing and opening times
     * @return array get Days in order
     */
    public function getClosingTimesStartArray($times)
    {
        $days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

        $beforeArray = [];
        $afterArray = [];
        $afterDay = 0;

        foreach ($days as $d) {
            if (strtolower($times->startDay) == strtolower($d)) {
                $afterDay = 1;
            }

            if ($afterDay == 1) {
                $afterArray[] = $d;
            } else {
                $beforeArray[] = $d;
            }
        }

        $daysOrder = array_merge($afterArray, $beforeArray);
        return $daysOrder;
    }

    /**
     * Get the date order in a format that javascript understands
     * getDateOrderJavascript
     *
     * @return string date order for javascri
     */
    public function getDateOrderJavascript()
    {
        $order = $this->helperdata->getConfig('catalog/custom_options/date_fields_order');
        if ($order == 'd,m,y') {
            return 'dd/mm/yy';
        } elseif ($order == 'm,d,y') {
            return 'mm/dd/yy';
        } else {
            return 'yy/mm/dd';
        }
    }

    /**
     * Return all the customer special dates tahter need like Easter Christamas...
     *
     * @param  int $locationId Id of the store location
     * @return objList
     */
    public function getSpecialClosingTimes($location_id)
    {
        $collection = $this->hoursFactory->create()->getCollection();
        $collection->addFieldToSelect('id');
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToSelect('day');
        $collection->addFieldToSelect('special_date');
        $collection->addFieldToSelect('start_time');
        $collection->addFieldToSelect('end_time');
        $collection->addFieldToSelect('type');
        $collection->addFieldToSelect('closed');
        $collection->addFieldToSelect('active');
        $collection->addFieldToFilter('location_id', ['eq' => $location_id]);
        $collection->addFieldToFilter('active', ['eq' =>1]);
        $collection->addFieldToFilter('special_date', ['neq' => 'NULL' ]);
        $collection->getSelect()->where('special_date > subdate(CURDATE(), 1)');
        $collection->setOrder('special_date', 'ASC');
        return $collection->load();
    }

    /**
     * Get closing times in editable format
     *
     * @param  int $locationId Id of the store location
     * @return obj closing times
     */
    public function getEditClosingTimes($location_id)
    {
        $calculateSpecialDates = false;
        return $this->getClosingTimes($location_id, $calculateSpecialDates, true);
    }

    public function getClosingTimesList($location_id)
    {
        $collection = $this->hoursFactory->create()->getCollection();
        $collection->addFieldToSelect('id');
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToSelect('day');
        $collection->addFieldToSelect('special_date');
        $collection->addFieldToSelect('start_time');
        $collection->addFieldToSelect('end_time');
        $collection->addFieldToSelect('type');
        $collection->addFieldToSelect('closed');
        $collection->addFieldToSelect('active');
        $collection->addFieldToFilter('location_id', ['eq' =>$location_id]);
        $collection->addFieldToFilter('active', ['eq'=>1]);
        $closingTimes = $collection->load();
        return $closingTimes;
    }

    /**
     * get closing times from the table in the database
     *
     * @param  int  $locationId            Id of the store location
     * @param  bool $calculateSpecialDates true is for displaying calculated special dates in frontend
     * @param  bool $editing               true is editing in backend
     * @return obj return closing times of all
     */
    public function getClosingTimes($location_id, $calculateSpecialDates, $editing)
    {
        //save the current timezone in the varraible
        $currentTimezone = date_default_timezone_get();

        //get the selected timezone from the database
        $timezone = $this->helperdata->getConfig('general/locale/timezone');

        if ($timezone != '' && $timezone != '0') {
            // change the timezone so all the data calculations are correct
            date_default_timezone_set($timezone);
        }
        $closingTimes = $this->getClosingTimesList($location_id);

        $specialDates = [];

        $times = $this->getWeeksTimesObject();
        if (!$editing) {
            if (empty($closingTimes)) {
                return null;
            }
        }

        foreach ($closingTimes as $ct) {
            $dayname = strtolower($ct['day']);
            $daysArray = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
            if (in_array($dayname, $daysArray)) {
                $times->{$dayname}->date = date('Y-n-d', strtotime('this '.$dayname));
                $times->{$dayname}->start_time = $ct['start_time'];
                $times->{$dayname}->end_time = $ct['end_time'];
                $times->{$dayname}->name = $ct['day'];
                $times->{$dayname}->closed = $ct['closed'];
                if ($times->{$dayname}->closed == 1) {
                    $times->{$dayname}->checked= ' checked="checked" ';
                    $times->{$dayname}->disabledTimes = ' disabled="disabled" ';
                } else {
                    $times->{$dayname}->checked = ' ';
                    $times->{$dayname}->disabledTimes = ' ';
                }
            }

            //if data need from the edit page
            if ($calculateSpecialDates == true) {
                //if the special date has a value in it
                if ($ct['special_date'] != '') {
                      //find what day of the week it is
                      $day = strtolower(date('l', strtotime($ct['special_date'])));

                      //see if that day of the week matches the ones it will display
                    if ($times->{$day}->date == date('Y-n-d', strtotime($ct['special_date']))) {
                        //override the day to be
                        $times->{$day}->start_time = $ct['start_time'];
                        $times->{$day}->end_time = $ct['end_time'];
                        $times->{$day}->name = $ct['day'];
                        $times->{$day}->special = true;
                    }
                }
            }
        }
        if ($calculateSpecialDates == true) {
            //find todays
            $times = $this->findTodays($times);
        }
          date_default_timezone_set($currentTimezone); // change back to normal

        return $times;
    }

    public function findTodays($times)
    {
            $day = strtolower(date('l', strtotime('now')));
            $times->{$day}->today = true;
            $times->startDay = $day;
            $day = strtolower(date('l', strtotime('+1 Day')));
            $times->{$day}->tomorrow = true;
            return $times;
    }

    public function getWeeksTimesObject()
    {
        $times = $this->stdClass->create();
        $times->monday =  $this->timeObject();
        $times->tuesday =  $this->timeObject();
        $times->wednesday =  $this->timeObject();
        $times->thursday =  $this->timeObject();
        $times->friday =  $this->timeObject();
        $times->saturday =  $this->timeObject();
        $times->sunday =  $this->timeObject();
        return $times;
    }

    /**
     * return a time oject to fall back on if no balues are ound or need to be created
     *
     * @return obj closing time object
     */
    function timeObject()
    {
        $time = $this->stdClass->create();
        $time->date = date('Y-n-d', strtotime('this sunday'));
        $time->start_time = '09:00';
        $time->end_time = '17:00';
        $time->disabledTimes  = '';
        $time->closed = false;
        return $time;
    }

    /**
     * Get the date order from Main Magento config settings
     *
     * @return string
     */
    public function getDateOrder()
    {
        $order = $this->helperdata->getConfig('catalog/custom_options/date_fields_order');
        return $order;
    }

    /**
     * Check if it is in a valid date format
     *
     * @param  string $date   datestring
     * @param  string $format format it is in
     * @return bool true is valid
     */
    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return ($d && $d->format($format) == $date);
    }

    /**
     * Check if the location Id exists in the database
     * @param  [int] $locationId [Location Id]
     * @return [bool]             [Check to see if the location Id Exists]
     */
    public function checkIfLocationIdExists($locationId)
    {
        $collection = $this->locateFactory->create()->getCollection();
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToFilter('location_id', ['eq' =>$locationId]);
        $results = $collection->load();
        foreach ($results as $r) {
            return true;
        }
        return false;
    }

    /**
     * Convert the main magento config settings date format to database format
     *
     * @param  datestring $date format of main magento config (catalog/custom_options/date_fields_order)
     * @return datestring for database Y-m-d
     */
    public function formatDateForDatabase($date)
    {
        if ($date == '') {
            return '';
        }
        if (strstr($date, '-')) { // make it so dashes work as well
            $date = str_replace('-', '/', $date);
        }

        $order = $this->getDateOrder();
        list($y,$m,$d) = explode('/', $date);
        if (strlen($y) == 4) { // check if first value has 4 for year
            return $y.'-'.$m.'-'.$d;
        }
        if ($order == 'd,m,y') {
            list($d,$m,$y) = explode('/', $date);
        } elseif ($order == 'm,d,y') {
            list($m,$d,$y) = explode('/', $date);
        } else {
            list($y,$m,$d) = explode('/', $date);
        }
        return $y.'-'.$m.'-'.$d;
    }

    /**
     * Format the date to be what is set in the main magento config settings
     *
     * @param  datestring $date Y-m-d format
     * @return datestring in correct format per store settings
     */
    public function formatDateForDatabaseReverse($date)
    {
        if ($date != '') {
            $order = $this->getDateOrder();
             list($y,$m,$d) = explode('-', $date);

            if ($order == 'd,m,y') {
                return $d.'/'.$m.'/'.$y;
            } elseif ($order == 'm,d,y') {
                return $m.'/'.$d.'/'.$y;
            } else {
                return $y.'/'.$m.'/'.$d;
            }
        } else {
            return '';
        }
    }

    /**
     * Update a single special time using in batch update special times
     *
     * @param  int $locationId Id of the store location
     * @param  obj $sTimes     object containing specail times
     * @return null
     */
    public function updateSingleSpecialTimes($locationId, $sTimes)
    {

        try {
            //check to make sure it isnt there

            //insert or update it

            //allways insert cause deleteing them and putting them back in

             //  //fix the date formatting
            $sTimes->date = $this->formatDateForDatabase($sTimes->date);
            $data = [
                'location_id' =>$locationId,
                'day' => $sTimes->dayname,
                'special_date' => $sTimes->date,
                'start_time' => $this->convertTimeTo24HourTime($sTimes->open_h, $sTimes->open_m, $sTimes->open_ampm),
                'end_time' => $this->convertTimeTo24HourTime($sTimes->close_h, $sTimes->close_m, $sTimes->close_ampm),
                'type' => 'special',
                'closed' => $sTimes->closed,
                'active' => '1'
            ];

            $collection = $this->hoursFactory->create()->getCollection();
            $collection->addFieldToSelect('id');
            $collection->addFieldToFilter('location_id', ['eq' =>$locationId]);
            $collection->addFieldToFilter('special_date', ['eq' =>$sTimes->date]);
            $results = $collection->load();
            foreach ($results as &$result) {
                $imported = 1;
                $data['id'] = $result->getId();
                $result->setData($data);
            }
            $results->save();

            if (!isset($imported)) {
                $this->hoursFactory->create()->setData($data)->save();
            }
            return true;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            throw new \Exception('Error update Single Special Times');
            return false;
        }
    }

    /**
     * update the special times into the database
     *
     * @param  array $specialTimes Array of objects
     * @param  int   $locationId   Id of the store location
     * @return null
     */
    public function updateSpecialTimes($specialTimes, $locationId)
    {

        try {
            //check if there are things to update
            if (empty($specialTimes) || $specialTimes == '') {
                //nothing to update
                return false;
            }

            $updateIds = [];
            $specialTimesMoreInfo = [];
            $newTimes = [];
            foreach ($specialTimes as $sTimes) {
                if ($sTimes->id != '') {
                    $updateIds[] = $sTimes->id;
                    $specialTimesMoreInfo[$sTimes->id] = $sTimes;
                } else {
                    $newTimes[] = $sTimes;
                }
            }

            //update all the social times and delete old ones
            //collection then delete
            $collection = $this->hoursFactory->create()->getCollection();
            $collection->addFieldToSelect('id');
            $collection->addFieldToFilter('location_id', ['eq' => $locationId]);
            $collection->addFieldToFilter('type', ['eq' => 'special']);
            $results = $collection->load();
            foreach ($results as &$result) {
                if (in_array($result->getId(), $updateIds)) {
                    //need to update it
                    $sTimes = $specialTimesMoreInfo[$result->getId()];
                    $result->setLocationId($locationId);
                    $result->setDay($sTimes->dayname);
                    $result->setSpecialDate($this->formatDateForDatabase($sTimes->date));
                    $starttime = $this->convertTimeTo24HourTime($sTimes->open_h, $sTimes->open_m, $sTimes->open_ampm);
                    $result->setStartTime($starttime);
                    $endTime = $this->convertTimeTo24HourTime($sTimes->close_h, $sTimes->close_m, $sTimes->close_ampm);
                    $result->setEndTime($endTime);
                    $result->setType('special');
                    $result->setClosed($sTimes->closed);
                    $result->setActive('1');
                } else {
                    //no longer needed
                    $result->delete();
                }
            }
            $results->save();

            $collection = $this->hoursFactory->create()->getCollection();
            //add in all the new lines
            foreach ($newTimes as $sTimes) {
                if (empty($sTimes) || !isset($sTimes->date)) {
                    continue;
                }

                $data = [
                'location_id' =>$locationId,
                'day' => $sTimes->dayname,
                'special_date' => $this->formatDateForDatabase($sTimes->date),
                'start_time' => $this->convertTimeTo24HourTime($sTimes->open_h, $sTimes->open_m, $sTimes->open_ampm),
                'end_time' => $this->convertTimeTo24HourTime($sTimes->close_h, $sTimes->close_m, $sTimes->close_ampm),
                'type' => 'special',
                'closed' => $sTimes->closed,
                'active' => '1'
                  ];
                $newTime = $this->hoursFactory->create()->setData($data);
                $collection->addItem($newTime);
            } // end forech
            $collection->save();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            throw new \Exception('Error update Special Times');
        }
    }

    /**
     * Update or Insert Closing Weekly Times
     *
     * @param  int    $locationId Id of the store location
     * @param  string $day        Name of the day
     * @param  obj    $times
     * @return bool
     */
    public function updateDayTimes($locationId, $day, $times)
    {
        try {
            $collection = $this->hoursFactory->create()->getCollection();
            $collection->addFieldToSelect('id');
            $collection->addFieldToFilter('location_id', ['eq'=>$locationId]);
            $collection->addFieldToFilter('day', ['eq' => $day]);
            $results = $collection->load();

            if (empty($results->getData())) { // it isnt already in the database
                $this->insertDayTimes($locationId, $day, $times); //insert as a new day
                return true;//dont contrinue script
            }
            foreach ($results as &$result) {
                $data = [
                'id' => $result->getId(),
                'special_date' => null,
                'day' => ucwords($day),
                'location_id' => $locationId,
                'start_time' => $this->convertTimeTo24HourTime($times->open_h, $times->open_m, $times->open_ampm),
                'end_time' => $this->convertTimeTo24HourTime($times->close_h, $times->close_m, $times->close_ampm),
                'type' => 'day',
                'closed' => $times->closed,
                'active' => '1'
                ];
                $result->setData($data);
            }
            $results->save();
              return true;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            throw new \Exception('Error update Day Times');
        }
    }

    /**
     * Insert Day Times into the database
     *
     * @param  int    $locationId Id of the store location
     * @param  string $day        day of the week
     * @param  obj    $times      object containing start and end times
     * @return null
     */
    public function insertDayTimes($locationId, $day, $times)
    {
        try {
            $data = [
                  'location_id' =>$locationId,
                  'day' => ucwords($day),
                  'special_date' => null,
                  'start_time' => $this->convertTimeTo24HourTime($times->open_h, $times->open_m, $times->open_ampm),
                  'end_time' => $this->convertTimeTo24HourTime($times->close_h, $times->close_m, $times->close_ampm),
                  'type' => 'day',
                  'closed' => $times->closed,
                  'active' => '1'
              ];
              $this->hoursFactory->create()->setData($data)->save();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            throw new \Exception('Error updating the weekly times');
        }
    }

    /**
     * update the Weekly times in the database
     *
     * @param  array $weeklyTimes Array of objects with Day=> DayTimes(obj)
     * @param  int   $locationId  Id of the store location
     * @return null
     */
    public function updateWeeklyTimes($weeklyTimes, $locationId)
    {
        if (empty($weeklyTimes) || $weeklyTimes == '') {
            return false;
        }
        try {
            $collection = $this->hoursFactory->create()->getCollection();
            $collection->addFieldToSelect('id');
            $collection->addFieldToSelect('day');
            $collection->addFieldToFilter('location_id', ['eq' => $locationId]);
            $collection->addFieldToFilter('type', ['eq' => 'day']);

            // print_r($collection->getSelect());
            $results = $collection->load();
            if (empty($results->getData())) {
                //insert
                $collection = $this->hoursFactory->create()->getCollection();
                foreach ($weeklyTimes as $day => $dayTimes) {
                    $data = [
                    'location_id' =>$locationId,
                    'day' => ucwords($day),
                    'special_date' => null,
                    'start_time' => $this->convertTimeTo24HourTime($dayTimes->open_h, $dayTimes->open_m, $dayTimes->open_ampm),
                    'end_time' => $this->convertTimeTo24HourTime($dayTimes->close_h, $dayTimes->close_m, $dayTimes->close_ampm),
                    'type' => 'day',
                    'closed' => $dayTimes->closed,
                    'active' => '1'
                      ];
                      $newTimes = $this->hoursFactory->create()->setData($data);
                      $collection->addItem($newTimes);
                }
                $collection->save();
            } else {
                $weeklyTimes = (array)$weeklyTimes;
                //update
                foreach ($results as &$result) {
                    $day = $result->getDay();
                    //dont need to load it again because already have it
                    $dayTimes = $weeklyTimes[strtolower($day)];
                    $data = [
                    'id' =>$result->getId(),
                    'location_id' =>$locationId,
                    'day' => ucwords($day),
                    'special_date' => null,
                    'start_time' => $this->convertTimeTo24HourTime($dayTimes->open_h, $dayTimes->open_m, $dayTimes->open_ampm),
                    'end_time' => $this->convertTimeTo24HourTime($dayTimes->close_h, $dayTimes->close_m, $dayTimes->close_ampm),
                    'type' => 'day',
                    'closed' => $dayTimes->closed,
                    'active' => '1'
                      ];
                    $result->setData($data);
                }
                $results->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical('Failed to Update Weekly Times - '.$e->getMessage());
            throw new \Exception(__('Failed to Update Weekly Times'));
        }
    }

    /**
     * Convert 12hour time to 24hour time
     *
     * @param  int    $h    hour
     * @param  int    $m    minute
     * @param  string $ampm 'AM' Or 'PM'
     * @return string H:i (Hour and Minute)
     */
    public function convertTimeTo24HourTime($h, $m, $ampm)
    {
        return date("H:i", strtotime($h.':'.$m.' '.$ampm));
    }

    /**
     * check the the database and see if that store location the is enabled and the store detailsa are viewable
     *
     * @param  int $storeid Id of the store location
     * @return bool true is saying it is visible
     */
    public function checkIfStoreLocationDetailsAreViewable($storeid)
    {
        $collection = $this->locateFactory->create()->getCollection();
        $collection->addFieldToSelect('location_id');
        $collection->addFieldToFilter('location_id', ['eq'=>$storeid]);
        $collection->addFieldToFilter('show_details_page', ['eq'=>1]);
        $collection->addFieldToFilter('active', ['eq'=>1]);
        $results = $collection->load();
        if (empty($results)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * [CheckIfPageUrlExistsAlready Check if the page url already exists ignore itself (id)]
     * @param [type] $url [description]
     * @param [type] $id  [description]
     */
    public function CheckIfPageUrlExistsAlready($url, $id)
    {
        if ($id == '') {
            $collection = $this->locateFactory->create()->getCollection();
            $collection->addFieldToSelect('location_id');
            $collection->addFieldToFilter('page_url', ['eq' => $url]);
            $results = $collection->load();
            if (!empty($results->getData())) {
                //a result was found
                return 'taken';
            } else {
                return '';
            }
        } else {
            //check if the url exest anywhere
            $collection = $this->locateFactory->create()->getCollection();
            $collection->addFieldToSelect('location_id');
            $collection->addFieldToFilter('page_url', ['eq' => trim($url)]);
            $results = $collection->load();
            if (!empty($results->getData())) {
                foreach ($results as $res) {
                    //check if it is just current id that is set
                    if ($res->getLocationId() != $id) {
                        return 'taken';
                    }
                }
                return 'same';
            } else {
                return '';
            }
        }
    }
}
