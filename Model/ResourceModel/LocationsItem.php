<?php
namespace Simpleplugz\Locations\Model\ResourceModel;

class LocationsItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('simpleplugz_locations', 'location_id');
    }
}
