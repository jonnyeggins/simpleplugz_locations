<?php
namespace Simpleplugz\Locations\Model\ResourceModel\LocationsHours;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Simpleplugz\Locations\Model\LocationsHours',
            'Simpleplugz\Locations\Model\ResourceModel\LocationsHours'
        );
    }
}
