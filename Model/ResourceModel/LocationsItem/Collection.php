<?php
namespace Simpleplugz\Locations\Model\ResourceModel\LocationsItem;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Simpleplugz\Locations\Model\LocationsItem', 'Simpleplugz\Locations\Model\ResourceModel\LocationsItem');
    }
    public function getCurrentPage()
    {
        return $this->getCurPage();
    }

    public function setCurrentPage($currentPage)
    {
        return $this->setCurPage($currentPage);
    }
}
