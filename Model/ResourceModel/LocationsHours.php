<?php
namespace Simpleplugz\Locations\Model\ResourceModel;

class LocationsHours extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('simpleplugz_locations_hours', 'id');   //here id is the primary key of custom table
    }
}
