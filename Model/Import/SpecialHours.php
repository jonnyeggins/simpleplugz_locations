<?php
namespace Simpleplugz\Locations\Model\Import;

use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;

class SpecialHours extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{
    const TABLE_ENTITY = 'simpleplugz_locations';

    public $_permanentAttributes = ['location_id','date','day','starttime','endtime','closed'];
    /**
     * If we should check column names
     *
     * @var bool
     */
    public $needColumnCheck = true;
    public $groupFactory;
    /**
     * Valid column names
     *
     * @array
     */
    public $validColumnNames = ['location_id','date','day','starttime','endtime','closed'];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    public $logInHistory = true;
    public $_validators = [];
    public $stdClass;
    public $locate;
    public $dataSourceModel;
    public $_importExportData;
    protected $_connection;
    protected $_resource;

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Simpleplugz\Locations\Model\SimpleplugzStdClass $stdClass,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Customer\Model\GroupFactory $groupFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
        $this->dataSourceModel = $importData;
        $this->stdClass = $stdClass;
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->errorAggregator = $errorAggregator;
        $this->locate = $locate;
        $this->groupFactory = $groupFactory;
    }
    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'store_locations_special_hours';
    }

    /**
     * Row validation.
     *
     * @param  array $rowData
     * @param  int   $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $behavior = \Magento\ImportExport\Model\Import::BEHAVIOR_DELETE;
        if ($behavior == $this->getBehavior()) {
             $this->addRowError('Delete Feature Turned Off', $rowNum);
                return false;
        }
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        // BEHAVIOR_DELETE use specific validation logic

        if (!isset($rowData['location_id']) || empty($rowData['location_id'])) {
            $this->addRowError('Must have location id', $rowNum);
            return false;
        }
        if (!$this->locate->checkIfLocationIdExists($rowData['location_id'])) {
            $this->addRowError('Location Id ('.$rowData['location_id'].') does not exist', $rowNum);
            return false;
        }
        if (!isset($rowData['day']) || empty($rowData['day'])) {
            $this->addRowError('Day must name a name', $rowNum);
            return false;
        }
        if (!isset($rowData['date']) || empty($rowData['date'])) {
            $this->addRowError('Must have a date', $rowNum);
            return false;
        }
            //check to make sture valid date
            $date = $this->locate->formatDateForDatabase($rowData['date']);
        if (!$this->locate->validateDate($date, 'Y-m-d')) {
            $this->addRowError('Date is invalid:'.$rowData['date'] . '= '.$date, $rowNum);
            return false;
        }
        if (!isset($rowData['closed']) || (empty($rowData['closed']) && $rowData['closed'] != 0)) {
            $this->addRowError('Must set day as 0 or 1', $rowNum);
            return false;
        }

        if ($rowData['closed'] == 0) {
            if (!$this->validateTimes($rowData, $rowNum)) {
                return false;
            }
        }
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    public function validateTimes($rowData, $rowNum)
    {
        //check startime is not empty
        if (!isset($rowData['starttime']) || empty($rowData['starttime'])) {
            $this->addRowError('Must have starttime if store is open', $rowNum);
            return false;
        }
        //check startime  is not invalid
        if (!$this->checkValidTime($rowData['starttime'])) {
            $this->addRowError('Start Time Invalid:'.$rowData['starttime'].'. Please use HH:MM format (24 Hour Time)', $rowNum);
            return false;
        }
        //check end time is not empty
        if (!isset($rowData['endtime']) || empty($rowData['endtime'])) {
            $this->addRowError('Must have endtime if store is open', $rowNum);
            return false;
        }
        //check end time is not invalid
        if (!$this->checkValidTime($rowData['endtime'])) {
            $this->addRowError('End Time Invalid:'.$rowData['endtime'].'. Please use HH:MM format (24 Hour Time)', $rowNum);
            return false;
        }
        return true;
    }

    public function checkValidTime($time)
    {
        if (preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $time)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Create Advanced price data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    public function _importData()
    {

        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            $this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;
    }
    /**
     * Save specialhours subscriber
     *
     * @return $this
     */
    public function saveEntity()
    {

        $this->saveAndReplaceEntity();
        return $this;
    }
    /**
     * Replace specialhours subscriber
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    /**
     * Deletes specialhours data from raw data.
     *
     * @return $this
     */
    public function deleteEntity()
    {
        throw new LocalizedException(__('Delete feature turned off'));
    }
    /**
     * Save and replace specialhours subscriber
     *
     * @return                                       $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function saveAndReplaceEntity()
    {

        $behavior = $this->getBehavior();

        $listTitle = [];
        while ($bunch = $this->dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_TITLE_IS_EMPTY, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                    //update
                    if (!$this->updateSpecialHours($rowData)) {
                        $this->addRowError('Could not import correctly', $rowNum);
                    }
                } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                     //Insert
                    if (!$this->importSpecialHours($rowData)) {
                        $this->addRowError('Could not import correctly', $rowNum);
                    }
                }
            }
        }

        return $this;
    }

    public function importSpecialHours($data)
    {

        $times = $this->stdClass->create();
        $times->dayname = $data['day'];
        $times->date = $data['date'];
        $times->open_h =  date("g", strtotime($data['starttime']));
        $times->open_m = date("i", strtotime($data['starttime']));
        $times->open_ampm = date("a", strtotime($data['starttime']));
        $times->close_h = date("g", strtotime($data['endtime']));
        $times->close_m = date("i", strtotime($data['endtime']));
        $times->close_ampm = date("a", strtotime($data['endtime']));
        $times->closed = $data['closed'];
        $locationId =  $data['location_id'];
        if (strstr($locationId, ',')) {
            //import multiple
            $locationsList = explode(',', $locationId);
            foreach ($locationsList as $locId) {
                if (!$this->locate->updateSingleSpecialTimes($locId, $times)) {
                    return false;
                }
            }
            return true;
        } else {
            if ($this->locate->updateSingleSpecialTimes($locationId, $times)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function updateSpecialHours($data)
    {
        $times = $this->stdClass->create();
        $times->dayname = $data['day'];
        $times->date = $data['date'];
        $times->open_h =  date("g", strtotime($data['starttime']));
        $times->open_m = date("i", strtotime($data['starttime']));
        $times->open_ampm = date("a", strtotime($data['starttime']));
        $times->close_h = date("g", strtotime($data['endtime']));
        $times->close_m = date("i", strtotime($data['endtime']));
        $times->close_ampm = date("a", strtotime($data['endtime']));
        $times->closed = $data['closed'];
        $locationId =  $data['location_id'];
        if (strstr($locationId, ',')) {
            //update multiple
            $locationsList = explode(',', $locationId);
            foreach ($locationsList as $locId) {
                if (!$this->locate->updateSingleSpecialTimes($locId, $times)) {
                    return false;
                }
            }
            return true;
        } else {
            if ($this->locate->updateSingleSpecialTimes($locationId, $times)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
