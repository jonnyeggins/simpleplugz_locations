<?php
namespace Simpleplugz\Locations\Model\Import;

use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class for importing Weekly Opening hours via CSV File
 */
class WeeklyHours extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{

    protected $_permanentAttributes = ['location_id','day','starttime','endtime','closed'];

    /**
     * If we should check column names
     *
     * @var bool
     */
    protected $needColumnCheck = true;
    protected $groupFactory;
    /**
     * Valid column names
     *
     * @array
     */

    protected $validColumnNames = ['location_id','day','starttime','endtime','closed'];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;

    protected $_validators = [];

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $locate;
    protected $stdClass;

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json
        \Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Simpleplugz\Locations\Model\SimpleplugzStdClass $stdClass,
        \Magento\Customer\Model\GroupFactory $groupFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->stdClass = $stdClass;
        $this->errorAggregator = $errorAggregator;
        $this->groupFactory = $groupFactory;
        $this->locate = $locate;
    }
    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'store_locations_hours';
    }

    /**
     * Row validation.
     *
     * @param  array $rowData
     * @param  int   $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
             $this->addRowError('Delete Feature Turned Off', $rowNum);
                return false;
        }
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        // BEHAVIOR_DELETE use specific validation logic
        if (!isset($rowData['location_id']) || empty($rowData['location_id'])) {
            $this->addRowError('Must have location id', $rowNum);
            return false;
        }
        if (!$this->locate->checkIfLocationIdExists($rowData['location_id'])) {
            $this->addRowError('Location Id ('.$rowData['location_id'].') Does not exist', $rowNum);
            return false;
        }
        if (!isset($rowData['day']) || empty($rowData['day'])) {
            $this->addRowError('Must have Day- e.g (Monday,Tuesday...', $rowNum);
            return false;
        }
        if (!isset($rowData['closed']) || (empty($rowData['closed']) && $rowData['closed'] != 0)) {
            $this->addRowError('Must set day as 0 or 1', $rowNum);
            return false;
        }

        if ($rowData['closed'] == 0) {
            if (!$this->validateTimes($rowData, $rowNum)) {
                return false;
            }
        }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    public function validateTimes($rowData, $rowNum)
    {
        //check startime is not empty
        if (!isset($rowData['starttime']) || empty($rowData['starttime'])) {
            $this->addRowError('Must have starttime if store is open', $rowNum);
            return false;
        }
        //check startime  is not invalid
        if (!$this->checkValidTime($rowData['starttime'])) {
            $this->addRowError('Start Time Invalid:'.$rowData['starttime'].'. Please use HH:MM format (24 Hour Time)', $rowNum);
            return false;
        }
        //check end time is not empty
        if (!isset($rowData['endtime']) || empty($rowData['endtime'])) {
            $this->addRowError('Must have endtime if store is open', $rowNum);
            return false;
        }
        //check end time is not invalid
        if (!$this->checkValidTime($rowData['endtime'])) {
            $this->addRowError('End Time Invalid:'.$rowData['endtime'].'. Please use HH:MM format (24 Hour Time)', $rowNum);
            return false;
        }
        return true;
    }

    public function checkValidTime($time)
    {
        if (preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $time)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Create Advanced price data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            $this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;
    }

     /**
      * Deletes  data from raw data.
      *
      * @return $this
      */
    public function deleteEntity()
    {
        throw new LocalizedException(__('Delete feature turned off'));
    }
    /**
     * Save newsletter subscriber
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    /**
     * Replace newsletter subscriber
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }

    /**
     * Save and replace newsletter subscriber
     *
     * @return                                       $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_TITLE_IS_EMPTY, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                    //update
                    $this->updateDayTimes($rowData);
                } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                     //Insert
                    $this->updateDayTimes($rowData);
                }
            }
        }
        return $this;
    }

    private function updateDayTimes($data)
    {
        $day = $data['day'];
        $times = $this->stdClass->create();
        $times->open_h =  date("g", strtotime($data['starttime']));
        $times->open_m = date("i", strtotime($data['starttime']));
        $times->open_ampm = date("a", strtotime($data['starttime']));
        $times->close_h = date("g", strtotime($data['endtime']));
        $times->close_m = date("i", strtotime($data['endtime']));
        $times->close_ampm = date("a", strtotime($data['endtime']));
        $times->closed = $data['closed'];
        $locationId =  $data['location_id'];
        if (strstr($locationId, ',')) {
            //update multiple
            $locationsList = explode(',', $locationId);
            foreach ($locationsList as $locId) {
                $this->locate->updateDayTimes($locId, $day, $times);
            }
        } else {
            $this->locate->updateDayTimes($locationId, $day, $times);
        }
    }

    /**
     * Validate data. (copied from parent AbstractEntity)
     *
     * @return ProcessingErrorAggregatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function validateData()
    {
        if (!$this->_dataValidated) {
            $this->getErrorAggregator()->clear();
            // do all permanent columns exist?
            $absentColumns = array_diff($this->_permanentAttributes, $this->getSource()->getColNames());

            if (!empty($absentColumns)){
                $cols = array();
                foreach ($absentColumns as $absent){
                    $cols[] = $absent;
                }
                $errorMessage = 'We can\'t find required columns: '.implode(', ',$cols);
               
                // $this->addErrors(implode(',',$absentColumns),'s');
                $this->addErrors($errorMessage,$cols);
                return $this->getErrorAggregator();
            }
            // $this->addErrors(self::ERROR_CODE_COLUMN_NOT_FOUND,$cols);
            if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE != $this->getBehavior()) {
                // check attribute columns names validity
                $columnNumber = 0;
                $emptyHeaderColumns = [];
                $invalidColumns = [];
                $invalidAttributes = [];
                foreach ($this->getSource()->getColNames() as $columnName) {
                    $columnNumber++;
                    if (!$this->isAttributeParticular($columnName)) {
                        if (trim($columnName) == '') {
                            $emptyHeaderColumns[] = $columnNumber;
                        } elseif (!preg_match('/^[a-z][a-z0-9_]*$/', $columnName)) {
                            $invalidColumns[] = $columnName;
                        } elseif ($this->needColumnCheck && !in_array($columnName, $this->getValidColumnNames())) {
                            $invalidAttributes[] = $columnName;
                        }
                    }
                }
                $this->addErrors(self::ERROR_CODE_INVALID_ATTRIBUTE, $invalidAttributes);
                $this->addErrors(self::ERROR_CODE_COLUMN_EMPTY_HEADER, $emptyHeaderColumns);
                $this->addErrors(self::ERROR_CODE_COLUMN_NAME_INVALID, $invalidColumns);
            }
            if (!$this->getErrorAggregator()->getErrorsCount()) {
                $this->_saveValidatedBunches();
                $this->_dataValidated = true;
            }
        }
        return $this->getErrorAggregator();
    }
}
