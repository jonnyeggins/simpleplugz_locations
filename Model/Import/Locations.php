<?php
namespace Simpleplugz\Locations\Model\Import;

// use Simpleplugz\Locations\Model\Import\CustomerGroup\RowValidatorInterface as ValidatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Filesystem\DirectoryList;

class Locations extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{

    const TITLE = 'customer_group_code';
    const TAX = 'tax_class_id';

    const TABLE_Entity = 'simpleplugz_locations';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */

    protected $_permanentAttributes =  ['location_id','location_name','location_address','geo_location_lat','geo_location_lng','custom_pointer','details_texts','show_details_page','show_closing_times','page_url','phone','email','image','show_manager','manager_image','manager_name','manager_email','manager_phone','active'];

    /**
     * If we should check column names
     *
     * @var bool
     */
    protected $needColumnCheck = true;
    protected $groupFactory;
    protected $httpFactory;
    protected $urlRewriteFactory;
    protected $urlRewriteCollection;
    /**
     * Valid column names
     *
     * @array
     */
    protected $validColumnNames = ['location_id','location_name','location_address','geo_location_lat','geo_location_lng','custom_pointer','details_texts','show_details_page','show_closing_times','page_url','phone','email','image','show_manager','manager_image','manager_name','manager_email','manager_phone','active'];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;

    protected $_validators = [];

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_connection;
    protected $_resource;
    protected $locate;
    protected $directory_list;
    protected $storeManager;
    protected $urlRewrite;

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json
        \Helper\Data $jsonHelper,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection $urlRewriteCollection,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->directory_list = $directory_list;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->httpFactory = $httpFactory;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->locate = $locate;
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteCollection = $urlRewriteCollection;
        $this->urlRewrite = $urlRewrite;
        $this->storeManager = $storeManager;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);

    }
    public function getValidColumnNames()
    {
        // echo 'getValidColumnNames';
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'store_locations';
    }

    /**
     * Row validation.
     *
     * @param  array $rowData
     * @param  int   $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $behavior = \Magento\ImportExport\Model\Import::BEHAVIOR_DELETE;
        if ($behavior == $this->getBehavior()) {
             $this->addRowError('Delete Feature Turned Off', $rowNum);
                return false;
        }
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        // BEHAVIOR_DELETE use specific validation logic
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            if (!isset($rowData['location_id']) || empty($rowData['location_id'])) {
                $this->addRowError('Must have location id', $rowNum);
                return false;
            }
        }

        if (!isset($rowData['location_name']) || empty($rowData['location_name'])) {
            $this->addRowError('Must have a Location Name', $rowNum);
            return false;
        }

        if (!isset($rowData['location_address']) || empty($rowData['location_address'])) {
            $this->addRowError('Must have a Location Address', $rowNum);
            return false;
        }

        if (!isset($rowData['active']) || (empty($rowData['active']) && $rowData['active'] != 0)) {
            $this->addRowError('Must have Active set 0 or 1', $rowNum);
            return false;
        }
        if (!$this->validateExtraInfo($rowData, $rowNum)) {
            return false;
        }
        if (!$this->validateImage($rowData['custom_pointer'], $rowNum)) {
            return false;
        }
        if (!$this->validateImage($rowData['image'], $rowNum)) {
            return false;
        }
        if (!$this->validateImage($rowData['manager_image'], $rowNum)) {
            return false;
        }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * Check to see if the image path exists and is allowed type
     * @param  [string] $filename [filename in the csv]
     * @param  [int] $rowNum   [row of the csv]
     * @return [bool]           [return if its passes or not]
     */
    public function validateImage($filename, $rowNum)
    {

        if ($filename != '') {
            if (!strstr('http', $filename)) {
                $imageDirectory = $this->directory_list->getRoot().'/'.$this->getImageTmpDir();
                $path = $imageDirectory.$filename;
            } else {
                $path = $filename;
            }
              $pathinfo = pathinfo($path);
              $imgExtensions = ['jpg', 'jpeg', 'gif', 'png'];
            if (!isset($pathinfo['extension']) || !in_array(strtolower($pathinfo['extension']), $imgExtensions)) {
                throw new LocalizedException(__('Please correct the image file type. ('.$filename.') on row '.$rowNum));
            }
            if (file_exists($path)) {
                return true;
            } else {
                throw new LocalizedException(__('Could not find image. ('.$filename.') on row '.$rowNum));
                return false;
            }
        }
        //images isnt required
        return true;
    }

    public function validateExtraInfo($rowData, $rowNum)
    {

        if (!isset($rowData['geo_location_lat']) || empty($rowData['geo_location_lat'])) {
            $this->addRowError('Must have a Geo Location (Lat)', $rowNum);
            return false;
        }

        if (!isset($rowData['geo_location_lng']) || empty($rowData['geo_location_lng'])) {
            $this->addRowError('Must have a Geo Location (Lng)', $rowNum);
            return false;
        }
        if (!isset($rowData['show_details_page']) || (empty($rowData['show_details_page']) && $rowData['show_details_page'] != 0)) {
            $this->addRowError('Must have Active set 0 or 1', $rowNum);
            return false;
        }
        if (!isset($rowData['show_manager']) || (empty($rowData['show_manager']) && $rowData['show_manager'] != 0)) {
            $this->addRowError('Must have Active set 0 or 1', $rowNum);
            return false;
        }
        if (!isset($rowData['show_closing_times']) || (empty($rowData['show_closing_times']) && $rowData['show_closing_times'] != 0)) {
            $this->addRowError('Must have Active set 0 or 1', $rowNum);
            return false;
        }
        if (!isset($rowData['page_url']) || empty($rowData['page_url'])) {
            $this->addRowError('Must have a unique page url', $rowNum);
            return false;
        }
        return true;
    }
    /**
     * Create Advanced price data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            $this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;
    }
    /**
     * Save locations 
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    /**
     * Replace locations 
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    /**
     * Deletes locations  data from raw data.
     *
     * @return $this
     */
    public function deleteEntity()
    {
        throw new LocalizedException(__('Delete feature turned off'));
    }
    /**
     * Save and replace locations 
     *
     * @return                                       $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError('Does not Validate', $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                    //update
                    $this->updateLocation($rowData);
                } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                     //Insert
                    $this->importLocation($rowData);
                }
            }
        }
        return $this;
    }

    private function importLocation($data)
    {
        $this->moveImages($data);
        $newId = $this->locate->importLocation($data);
        $this->addSeoUrlForStoreId($newId, $data['page_url']);
    }

    private function updateLocation($data)
    {
        $this->moveImages($data);
        $this->locate->updateLocation($data);
        $this->addSeoUrlForStoreId($data['location_id'], $data['page_url']);
    }

    public function moveImages($data)
    {
        $imageDirectory = $this->directory_list->getRoot().'/'.$this->getImageTmpDir();
        $this->copyImage($imageDirectory, $data['custom_pointer']);
        $this->copyImage($imageDirectory, $data['image']);
        $this->copyImage($imageDirectory, $data['manager_image']);
    }

    private function getImageTmpDir()
    {
        return $this->getParameters()['import_images_file_dir'];
    }

    public function copyImage($imageDirectory, $filename)
    {
        if ($filename != '') {
            $path = $imageDirectory.$filename;
            $pathinfo = pathinfo($path);
            $imgExtensions = ['jpg', 'jpeg', 'gif', 'png'];
            if (!isset($pathinfo['extension']) || !in_array(strtolower($pathinfo['extension']), $imgExtensions)) {
                throw new LocalizedException(__('Please correct the image file type. ('.$filename.')'));
            }

            $destination = 'stores/'.$filename;
            if ($path != $destination) {
                $content = file_get_contents($path);
                $results = $this->mediaDirectory->writeFile($destination, $content);
            }
        }


        if ($filename != '') {
            if (!strstr('http', $filename)) {
                $imageDirectory = $this->directory_list->getRoot().'/'.$this->getImageTmpDir();
                $path = $imageDirectory.$filename;
            } else {
                $path = $filename;
            }
                $pathinfo = pathinfo($path);
                $imgExtensions = ['jpg', 'jpeg', 'gif', 'png'];
            if (!isset($pathinfo['extension']) || !in_array(strtolower($pathinfo['extension']), $imgExtensions)) {
                throw new LocalizedException(__('Please correct the image file type. ('.$filename.') on row '.$rowNum));
            }
            if (file_exists($path)) {
                return true;
            } else {
                throw new LocalizedException(__('Could not find image. ('.$filename.') on row '.$rowNum));
                return false;
            }
        }
    }

    /**
     *  Add URLrewrite rule to the database to alias a url to system url (for the full store details page)
     * @param [type] $storeid [location id]
     * @param [type] $url     [url to pretent to be located at]
     */
    private function addSeoUrlForStoreId($storeid, $url)
    {
        //check if the store id exists already
        if (!$this->checkForCurrentSeoUrl("locations/store/index/storeid/".$storeid, $url)) {
            //doest exist need to create it
            $urlRewriteModel = $this->urlRewriteFactory->create();
            /* set current store id */
            $urlRewriteModel->setStoreId($this->storeManager->getStore()->getId()); // this magneot store id not location id
            /* this url is not created by system so set as 0 */
            $urlRewriteModel->setIsSystem(0);
            /* unique identifier - set random unique value to id path */
            $urlRewriteModel->setIdPath(rand(1, 100000));
            /* set actual url path to target path field */
            $urlRewriteModel->setTargetPath("locations/store/index/storeid/".$storeid);
            /* set requested path which you want to create */
            $urlRewriteModel->setRequestPath($url);
            /* set current store id */
            $urlRewriteModel->save();
        }
    }

    /**
     *  Check to see if there is already a URLrewrite record in the database
     * @param  [string] $target     [targetparh of the url pretesnds to be]
     * @param  [string] $requestUrl [url that is request by the user]
     * @return [bool]             [true if the record is already in the database]
     */
    private function checkForCurrentSeoUrl($target, $requestUrl)
    {
        $urlCollection = $this->urlRewriteFactory->create()->getCollection();
        $urlCollection->addFieldToFilter('target_path', $target);
        foreach ($urlCollection as $url) {
            if ($url->getData('request_url') == $requestUrl) {
                //the url exists already so n need to create it
                return true;
            }
            $this->deleteSeoUrl($url->getData('url_rewrite_id'));
        }
        return false;
    }

    /**
     *  Delete the URLRewrite record form the database
     * @param  [int] $urlRewriteId [URLrewrite Id in the database]
     * @return [null]               [return not an error]
     */
    private function deleteSeoUrl($urlRewriteId)
    {
        $this->urlRewrite->load($urlRewriteId);
        try {
            $this->urlRewrite->delete();
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t delete URL Rewrite right now.'));
            $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
    }

    

    /**
     * Validate data. (copied from parent AbstractEntity)
     *
     * @return ProcessingErrorAggregatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function validateData()
    {
        if (!$this->_dataValidated) {
            $this->getErrorAggregator()->clear();
            // do all permanent columns exist?
            $absentColumns = array_diff($this->_permanentAttributes, $this->getSource()->getColNames());

            if (!empty($absentColumns)){
                $cols = array();
                foreach ($absentColumns as $absent){
                    $cols[] = $absent;
                }
                $errorMessage = 'We can\'t find required columns: '.implode(', ',$cols);
               
                // $this->addErrors(implode(',',$absentColumns),'s');
                $this->addErrors($errorMessage,$cols);
                return $this->getErrorAggregator();
            }
            // $this->addErrors(self::ERROR_CODE_COLUMN_NOT_FOUND,$cols);
            if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE != $this->getBehavior()) {
                // check attribute columns names validity
                $columnNumber = 0;
                $emptyHeaderColumns = [];
                $invalidColumns = [];
                $invalidAttributes = [];
                foreach ($this->getSource()->getColNames() as $columnName) {
                    $columnNumber++;
                    if (!$this->isAttributeParticular($columnName)) {
                        if (trim($columnName) == '') {
                            $emptyHeaderColumns[] = $columnNumber;
                        } elseif (!preg_match('/^[a-z][a-z0-9_]*$/', $columnName)) {
                            $invalidColumns[] = $columnName;
                        } elseif ($this->needColumnCheck && !in_array($columnName, $this->getValidColumnNames())) {
                            $invalidAttributes[] = $columnName;
                        }
                    }
                }
                $this->addErrors(self::ERROR_CODE_INVALID_ATTRIBUTE, $invalidAttributes);
                $this->addErrors(self::ERROR_CODE_COLUMN_EMPTY_HEADER, $emptyHeaderColumns);
                $this->addErrors(self::ERROR_CODE_COLUMN_NAME_INVALID, $invalidColumns);
            }
            if (!$this->getErrorAggregator()->getErrorsCount()) {
                $this->_saveValidatedBunches();
                $this->_dataValidated = true;
            }
        }
        return $this->getErrorAggregator();
    }


}
