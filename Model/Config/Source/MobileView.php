<?php
 
//Location: magento2_root/app/code/Simpleplugz/Location/Model/Config/Source/Custom.php
namespace Simpleplugz\Locations\Model\Config\Source;

class MobileView implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
 
        return [
            ['value' => 'split', 'label' => __('Split Into Tabs')],
            ['value' => 'fullwidth', 'label' => __('Show list under map')]
        ];
    }
}
