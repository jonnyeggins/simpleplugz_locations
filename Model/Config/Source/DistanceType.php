<?php
 
//Location: magento2_root/app/code/Simpleplugz/Location/Model/Config/Source/Custom.php
namespace Simpleplugz\Locations\Model\Config\Source;

class DistanceType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
 
        return [
            ['value' => 'kms', 'label' => __('Kilometres')],
            ['value' => 'miles', 'label' => __('Miles')]
        ];
    }
}
