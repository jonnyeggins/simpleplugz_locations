<?php
 
//Location: magento2_root/app/code/Simpleplugz/Location/Model/Config/Source/Custom.php
namespace Simpleplugz\Locations\Model\Config\Source;

class Custom implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
 
        return [
            ['value' => 0, 'label' => __('Popup')],
            ['value' => 1, 'label' => __('Expand')],
            ['value' => 2, 'label' => __('Large Map')]
        ];
    }
}
