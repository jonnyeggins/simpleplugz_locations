<?php
 
//Location: magento2_root/app/code/Egg/Location/Model/Config/Source/Custom.php
namespace Simpleplugz\Locations\Model\Config\Source;

class Timezones implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        $zones = timezone_identifiers_list();
        $timezones = [];
        foreach ($zones as $zone) {
            $timezones[] = $zone;
        }
        return $timezones;
    }
}
