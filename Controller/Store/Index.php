<?php
namespace Simpleplugz\Locations\Controller\Store;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->locate = $locate;
        return parent::__construct($context);
    }
    
    public function execute()
    {
       
        //get the store id
         $storeid = $this->getRequest()->getParam('storeid');
        if ($storeid == '') {
            $storeUrl = $this->getRequest()->getParam('store');
            $storeid = $this->locate->getStoreIdFromUrl($storeUrl);
        }
        if (!is_numeric($storeid)) {
             $this->_redirect('locations');
        }
        //go a check to see if store details is viewable
        if (!$this->locate->checkIfStoreLocationDetailsAreViewable($storeid)) {
            //the storeid is not viewable
            //redirect to locations page
            $this->_redirect('locations');
        }

        //continue on
        return $this->resultPageFactory->create();
    }
}
