<?php
namespace Simpleplugz\Locations\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $locate;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->locate = $locate;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}
