<?php
namespace Simpleplugz\Locations\Controller\Index;

/**
 * Get settings from the database for the store finder
 */
class getSettingsFromDatabase extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->locate = $locate;
        return parent::__construct($context);
    }

    /**
     * Get Settings json
     *
     * @return json settings for javascript
     */
    public function execute()
    {

        $settings = $this->locate->getSettingsFromDatabase();
        $jsonCreate = $this->resultJsonFactory->create();
        return $jsonCreate->setData($settings);
    }
}
