<?php
namespace Simpleplugz\Locations\Controller\Index;

/**
 * Get all locations in json format for the map pinpoints
 */
class getAllLocations extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $resultJsonFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->locate = $locate;
        return parent::__construct($context);
    }

    /**
     * Get all locations in json format for the map pinpoints
     *
     * @return [type] [description]
     */
    public function execute()
    {
        $locations = $this->locate->getAllLocations();
        $jsonCreate = $this->resultJsonFactory->create();
        return $jsonCreate->setData($locations);
    }
}
