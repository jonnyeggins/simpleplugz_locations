<?php
namespace Simpleplugz\Locations\Controller\Index;

class getStoreDetailsSettings extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $resultJsonFactory;
    protected $locate;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->locate = $locate;
        return parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $storeid = $post['storeid'];
        $detailsSettings = $this->locate->getStoreDetailsSettings($storeid);

        $jsonCreate = $this->resultJsonFactory->create();
        return $jsonCreate->setData($detailsSettings);
    }
}
