<?php
namespace Simpleplugz\Locations\Controller\Index;

/**
 * Get Locations based off Geo Co-ords
 */
class getLocationsFromGeo extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->locate = $locate;
        return parent::__construct($context);
    }

    /**
     * Get Locations List template
     *
     * @return html
     */
    public function execute()
    {
         $layout = $this->_view->getLayout();
         $block = $layout->createBlock('Simpleplugz\Locations\Block\LocationsList');
         $htmlBlock = $block->setTemplate("Simpleplugz_Locations::locationsList.phtml")->toHtml();
        $this->getResponse()->setBody($htmlBlock);
    }
}
