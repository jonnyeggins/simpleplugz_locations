<?php
namespace Simpleplugz\Locations\Controller\Adminhtml\Index;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var \Simpleplugz\Locations\Model\Index
     */
    public $model;
    public $fileUploaderFactory;
    public $httpFactory;
    public $locate;
    public $urlRewriteFactory;
    public $urlRewrite;
    public $locate_helper;
    public $urlRewriteCollection;

    public $imageSize   = [
        'minheight'     => 50,
        'minwidth'      => 50,
        'maxheight'     => 800,
        'maxwidth'      => 1024,
    ];

    /**
     * @param Action\Context                     $context
     * @param \Simpleplugz\Locations\Model\Index $model
     */
    public function __construct(
        Action\Context $context,
        \Simpleplugz\Locations\Model\LocationsItemFactory $model,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Simpleplugz\Locations\Helper\Data $locate_helper,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection $urlRewriteCollection,
        \Simpleplugz\Locations\Model\Locate $locate
    ) {
        parent::__construct($context);
        $this->model = $model;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->httpFactory = $httpFactory;
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewrite = $urlRewrite;
        $this->urlRewriteCollection = $urlRewriteCollection;
        $this->storeManager = $storeManager;
        $this->locate_helper = $locate_helper;
        $this->locate = $locate;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Simpleplugz_Locations::storemanager');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $data = $this->getRequest()->getPostValue();
        /**
 * @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect
*/
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            /**
 * @var \Simpleplugz\Locations\Model\Index $model
*/
            $model = $this->model->create();

             /************ *********/
            //store image
              // save image data and remove from data array
            if (isset($data['image'])) {
                $imageData = $data['image'];
                unset($data['image']);
            } else {
                $imageData = [];
            }

            //manager image
            if (isset($data['manager_image'])) {
                $manImageData = $data['manager_image'];
                unset($data['manager_image']);
            } else {
                $manImageData = [];
            }

            //custom pointer small image
            if (isset($data['custom_pointer'])) {
                $cusSmallImageData = $data['custom_pointer'];
                unset($data['custom_pointer']);
            } else {
                $cusSmallImageData = [];
            }

             /************ *********/

            $model->setData($data);
            if (!isset($data['location_id'])) {
                $data['location_id'] = '';
                $new = 1;
            }

            if ($data['show_details_page'] == 1) {
                if (isset($data['page_url'])) {
                    if ($data['page_url'] == '') {
                        $this->messageManager->addError('Store URL Required');
                        $this->_getSession()->setFormData($data);
                        return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
                    } else {
                        $checkedUrl = $this->locate->CheckIfPageUrlExistsAlready($data['page_url'], $data['location_id']);
                        if ($checkedUrl == 'taken') {
                            $this->messageManager->addError('Store URL already used . '.$data['location_id']);
                            $this->_getSession()->setFormData($data);
                            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $data['location_id']]);
                        } else {
                            if ($data['location_id'] != '') {
                                $previousUrl = $this->getCurrentUrlFromLocationId($data['location_id']);
                            }
                            $model->setPageUrl($data['page_url']);
                        }
                        //put url in later
                    }
                }
            }

            $this->_eventManager->dispatch(
                'locations_index_prepare_save',
                ['index' => $model, 'request' => $this->getRequest()]
            );

            try {
                //store image
                $imageHelper = $this->locate_helper;
                if (isset($imageData['delete']) && $model->getImage()) {
                    $imageHelper->removeImage($model->getImage());
                    $model->setImage(null);
                }

                //manager image
                $manImageHelper = $this->locate_helper;
                if (isset($manImageData['delete']) && $model->getManagerImage()) {
                    $manImageHelper->removeImage($model->getManagerImage());
                    $model->setManagerImage(null);
                }

                //custom pointer small image
                $cusSmallImageHelper = $this->locate_helper;
                if (isset($cusSmallImageData['delete']) && $model->getData('custom_pointer')) {
                    $cusSmallImageHelper->removeImage($model->getData('custom_pointer'));
                    $model->setData('custom_pointer', null);
                }

                /************ *********/
                //location image
                $imageFile = $imageHelper->uploadImage('image');
                if ($imageFile) {
                    $imageFile  = 'stores/'.$imageFile;
                    $model->setImage($imageFile);
                }

                //manager image
                $manImageFile = $manImageHelper->uploadImage('manager_image');
                if ($manImageFile) {
                    $manImageFile  = 'stores/'.$manImageFile;
                    $model->setData('manager_image', $manImageFile);
                }

                //custom pointer small image
                $cusSmallImageFile = $cusSmallImageHelper->uploadImage('custom_pointer');
                if ($cusSmallImageFile) {
                    $cusSmallImageFile  = 'stores/'.$cusSmallImageFile;
                    $model->setData('custom_pointer', $cusSmallImageFile);
                }
                
                if (isset($new)) {
                    //set defaults
                    $model->setData('default', '0');
                }

                $model->save();
                if ($data['show_details_page'] == 1) {
                    if ($data['show_closing_times'] == 1) {
                        //Save the closing times
                        $this->saveClosingTimes($data, $model->getId());
                    }

                    if ($data['location_id'] == '') {
                         //add in new
                        $this->addSeoUrlForStoreId($model->getLocationId(), $data['page_url']);
                    } elseif (isset($previousUrl) && $previousUrl != $data['page_url']) {
                        // //add in new url if different
                        $this->addSeoUrlForStoreId($model->getLocationId(), $data['page_url']);
                    } else {
                        //no need to add in url again
                    }
                }
                $this->messageManager->addSuccess(__('Location saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the index').$e->getMessage());
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    protected function saveClosingTimes($data, $id)
    {

        $ctimeString = $data['closing_times'];
        $cTimeObj = json_decode($ctimeString);
        $stimeString = $data['closing_times_special'];
        $sTimeObj = json_decode($stimeString);

        $this->locate->updateWeeklyTimes($cTimeObj, $id);
        $this->locate->updateSpecialTimes($sTimeObj, $id);
    }

    protected function getCurrentUrlFromLocationId($id)
    {
        return $this->model->create()->load($id)->getPageUrl();
    }

    /**
     *  Add URLrewrite rule to the database to alias a url to system url (for the full store details page)
     * @param [type] $storeid [location id]
     * @param [type] $url     [url to pretent to be located at]
     */
    protected function addSeoUrlForStoreId($storeid, $url)
    {
        //check if the store id exists already
        if (!$this->checkForCurrentSeoUrl("locations/store/index/storeid/".$storeid, $url)) {
            //doest exist need to create it
            $urlRewriteModel = $this->urlRewriteFactory->create();
            /* set current store id */
            $urlRewriteModel->setStoreId($this->storeManager->getStore()->getId()); // this magneot store id not location id
            /* this url is not created by system so set as 0 */
            $urlRewriteModel->setIsSystem(0);
            /* unique identifier - set random unique value to id path */
            $urlRewriteModel->setIdPath(rand(1, 100000));
            /* set actual url path to target path field */
            $urlRewriteModel->setTargetPath("locations/store/index/storeid/".$storeid);
            /* set requested path which you want to create */
            $urlRewriteModel->setRequestPath($url);
            /* set current store id */
            $urlRewriteModel->save();
        }
    }

    /**
     *  Check to see if there is already a URLrewrite record in the database
     * @param  [string] $target     [targetparh of the url pretesnds to be]
     * @param  [string] $requestUrl [url that is request by the user]
     * @return [bool]             [true if the record is already in the database]
     */
    protected function checkForCurrentSeoUrl($target, $requestUrl)
    {
        $urlCollection = $this->urlRewriteFactory->create()->getCollection();
        $urlCollection->addFieldToFilter('target_path', $target);
        foreach ($urlCollection as $url) {
            if ($url->getData('request_url') == $requestUrl) {
                //the url exists already so n need to create it
                return true;
            }
            $this->deleteSeoUrl($url->getData('url_rewrite_id'));
        }
        return false;
    }

    /**
     *  Delete the URLRewrite record form the database
     * @param  [int] $urlRewriteId [URLrewrite Id in the database]
     * @return [null]               [return not an error]
     */
    protected function deleteSeoUrl($urlRewriteId)
    {
        $this->urlRewrite->load($urlRewriteId);
        try {
            $this->urlRewrite->delete();
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t delete URL Rewrite right now.'));
            $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
    }

    /**
     * Upload image and return uploaded image file name or false
     *
     * @throws Mage_Core_Exception
     * @param  string $scope the request key for file
     * @return bool|string
     */
    protected function uploadImage($scope)
    {
        $adapter = $this->httpFactory->create();
        $adapter->addValidator(new \Zend_Validate_File_ImageSize($this->imageSize));
        $adapter->addValidator(
            new \Zend_Validate_File_FilesSize(['max' => 1048576])
        );

        if ($adapter->isUploaded($scope)) {
            // validate image
            if (!$adapter->isValid($scope)) {
                throw new \Magento\Framework\Model\Exception(__('Uploaded image is not valid.'));
            }

            $uploader = $this->fileUploaderFactory->create(['fileId' => $scope]);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $uploader->setAllowCreateFolders(true);

            if ($uploader->save($this->getBaseDir())) {
                return $uploader->getUploadedFileName();
            }
        }
        return false;
    }
}
