<?php
namespace Simpleplugz\Locations\Controller\Adminhtml\Index;

class Index extends \Magento\Backend\App\Action
{
    public $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        return parent::__construct($context);
    }
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Simpleplugz_Locations::index');
        $resultPage->addBreadcrumb(
            __('Manage index'),
            __('Manage index')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Store Locations'));
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Simpleplugz_Locations::storemanager');
    }
}
