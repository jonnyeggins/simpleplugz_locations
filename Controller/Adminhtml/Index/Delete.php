<?php
namespace Simpleplugz\Locations\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

class Delete extends Action
{
    public $model;

    /**
     * @param Action\Context                          $context
     * @param \Simpleplugz\Locations\Model\Department $model
     */
    public function __construct(
        Action\Context $context,
        \Simpleplugz\Locations\Model\LocationsItemFactory $model
    ) {
        parent::__construct($context);
        $this->model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Simpleplugz_Locations::delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /**
 * @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect
*/
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->model;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Location deleted'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('Department does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}
