<?php

namespace Simpleplugz\Locations\Controller\Adminhtml\Mass;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\ConvertToCsv;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;

class MassDisable extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var MetadataProvider
     */
    protected $metadataProvider;

    /**
     * @var WriteInterface
     */
    protected $directory;

    /**
     * @var ConvertToCsv
     */
    protected $converter;

    /**
     * @var FileFactory
     */
    protected $fileFactory;
  
    protected $locateFactory;

    /**
     * @param Context           $context
     * @param Filter            $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        Filesystem $filesystem,
        ConvertToCsv $converter,
        FileFactory $fileFactory,
        \Magento\Ui\Model\Export\MetadataProvider $metadataProvider,
        \Simpleplugz\Locations\Model\Locate $resource,
        \Simpleplugz\Locations\Model\LocationsItemFactory $locateFactory
    ) {

        $this->resources = $resource;
        $this->filter = $filter;
        $this->_connection = $resource->getConnection('write');
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->metadataProvider = $metadataProvider;
        $this->converter = $converter;
        $this->fileFactory = $fileFactory;
        $this->locateFactory = $locateFactory;
        parent::__construct($context);
    }
     /**
      * {@inheritdoc}
      */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Simpleplugz_Locations::storemanager');
    }

    /**
     * Export selected data.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        try {
            $posted = $this->getRequest()->getPost();
            if (!isset($posted['selected'])) {
                $this->messageManager->addError('error nothing selected');
                return false;
            }
            $selected = $posted['selected'];
            $idsList = [];
            foreach ($selected as $sel) {
                if (!is_numeric($sel)) {
                    $this->messageManager->addError('number not selected');
                    return false;
                }
                $idsList[] = (int)$sel;
            }
            if (!empty($idsList)) {
                $collection = $this->locateFactory->create()->getCollection();
                $collection->addFieldToSelect('location_id');
                $collection->addFieldToFilter('location_id', ['in' => $idsList]);
                    $results = $collection->load();
                foreach ($results as &$result) {
                    $result->setData('active', '0');
                }
                    $results->save();
            }
            $this->messageManager->addSuccess('Selected Store(s) Disabled');
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __(
                        'S
                Failed to Disable'
                    ).$e->getMessage()
                );
        }
    }
}
