<?php

namespace Simpleplugz\Locations\Controller\Adminhtml\Mass;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\ConvertToCsv;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;

class MassExport extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var MetadataProvider
     */
    protected $metadataProvider;

    /**
     * @var WriteInterface
     */
    protected $directory;

    /**
     * @var ConvertToCsv
     */
    protected $converter;

    /**
     * @var FileFactory
     */
    protected $fileFactory;
    private $locateFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    protected $request;
    public function __construct(
        Context $context,
        Filter $filter,
        Filesystem $filesystem,
        ConvertToCsv $converter,
        FileFactory $fileFactory,
        \Magento\Ui\Model\Export\MetadataProvider $metadataProvider,
        \Simpleplugz\Locations\Model\Locate $resource,
        \Simpleplugz\Locations\Model\LocationsItemFactory $locateFactory
    ) {

        $this->resources = $resource;
        $this->filter = $filter;
        $this->locateFactory = $locateFactory;
        $this->_connection = $resource->getConnection('write');
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->metadataProvider = $metadataProvider;
        $this->converter = $converter;
        $this->fileFactory = $fileFactory;

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Simpleplugz_Locations::storemanager');
    }

    /**
     * Export selected data.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {

        try {
            $headings = ['location_id',
            'location_name',
            'location_address',
            'geo_location_lat',
            'geo_location_lng',
            'custom_pointer',
            'details_texts',
            'show_details_page',
            'page_url',
            'show_closing_times',
            'phone',
            'email',
            'image',
            'show_manager',
            'manager_image',
            'manager_name',
            'manager_email',
            'manager_phone',
            'active'];

            $name = 'Exported';
            $file = 'export/Locations_' . $name . '.csv';
            $this->directory->create('export');
            $stream = $this->directory->openFile($file, 'w+');
            $stream->lock();
            $stream->writeCsv($headings);

            $posted = $this->getRequest()->getPost();

            if (!isset($posted['selected'])) {
                $this->messageManager->addError('error nothing selected');
                return false;
            }
            $selected = $posted['selected'];
            $idsList = [];
            foreach ($selected as $sel) {
                if (!is_numeric($sel)) {
                    $this->messageManager->addError('number not selected');
                    return false;
                }
                $idsList[] = (int)$sel;
            }

            if (!empty($idsList)) {
                $collection = $this->locateFactory->create()->getCollection();
                $collection->addFieldToSelect('location_id');
                $collection->addFieldToSelect('location_name');
                $collection->addFieldToSelect('location_address');
                $collection->addFieldToSelect('geo_location_lat');
                $collection->addFieldToSelect('geo_location_lng');
                $collection->addFieldToSelect('custom_pointer');
                $collection->addFieldToSelect('details_texts');
                $collection->addFieldToSelect('show_details_page');
                $collection->addFieldToSelect('page_url');
                $collection->addFieldToSelect('show_closing_times');
                $collection->addFieldToSelect('phone');
                $collection->addFieldToSelect('email');
                $collection->addFieldToSelect('image');
                $collection->addFieldToSelect('show_manager');
                $collection->addFieldToSelect('manager_image');
                $collection->addFieldToSelect('manager_name');
                $collection->addFieldToSelect('manager_email');
                $collection->addFieldToSelect('manager_phone');
                $collection->addFieldToSelect('active');
                $collection->addFieldToFilter('location_id', ['in' => $idsList]);
                $results = $collection->load();
                foreach ($results as $result) {
                    $data = $result->getData();
                    //write data to file
                    $stream->writeCsv($data);
                }
            }

            $stream->unlock();
            $stream->close();
            return $this->fileFactory->create(
                'export.csv',
                [
                'type' => 'filename',
                'value' => $file,
                'rm' => true  // can delete file after use
                ],
                'var'
            );
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError('could not export. | Reason '.$e->getMessage());
            return false;
        }
    }
}
