<?php
namespace Simpleplugz\Locations\Setup;

use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        //add locations table to the database
        if (!$installer->tableExists('simpleplugz_locations')) {
                $table = $installer->getConnection()
                    ->newTable($installer->getTable('simpleplugz_locations'));
                $table->addColumn(
                    'location_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                        ],
                    'Location Id'
                )
                    ->addColumn(
                        'location_name',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable'  => false,],
                        'Store Name'
                    )
                    ->addColumn(
                        'location_address',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable'  => false,],
                        'LocationAddress'
                    )
                    ->addColumn(
                        'geo_location_lat',
                        Table::TYPE_DECIMAL,
                        '10,8',
                        ['unsigned' => true,'precision' => 11,'scale' => 8],
                        'geo_location_lat'
                    )
                    ->addColumn(
                        'geo_location_lng',
                        Table::TYPE_DECIMAL,
                        '10,8',
                        ['unsigned' => true,'precision' => 11,'scale' => 8],
                        'geo_location_lng'
                    )
                    ->addColumn(
                        'custom_pointer',
                        Table::TYPE_TEXT,
                        null,
                        [],
                        'custom_pointer'
                    )
                    ->addColumn(
                        'details_texts',
                        Table::TYPE_TEXT,
                        '2M',
                        [],
                        'other_texts'
                    )
                    ->addColumn(
                        'show_details_page',
                        Table::TYPE_INTEGER,
                        '2',
                        [],
                        'show_details_page'
                    )
                    ->addColumn(
                        'page_url',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable'  => true,],
                        'page_url'
                    )
                    ->addColumn(
                        'show_closing_times',
                        Table::TYPE_INTEGER,
                        '2',
                        [],
                        'show_closing_times'
                    )
                    ->addColumn(
                        'phone',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'phone'
                    )
                    ->addColumn(
                        'email',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'email'
                    )
                    ->addColumn(
                        'image',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'image'
                    )
                    ->addColumn(
                        'show_manager',
                        Table::TYPE_INTEGER,
                        '2',
                        [],
                        'show_manager'
                    )
                    ->addColumn(
                        'manager_image',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'manager_image'
                    )
                    ->addColumn(
                        'manager_name',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'manager_name'
                    )
                    ->addColumn(
                        'manager_email',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'manager_email'
                    )
                    ->addColumn(
                        'manager_phone',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'manager_phone'
                    )
                    ->addColumn(
                        'active',
                        Table::TYPE_INTEGER,
                        '2M',
                        [],
                        'active'
                    )
                    ->addColumn(
                        'default',
                        Table::TYPE_INTEGER,
                        '2M',
                        [],
                        'default'
                    )
                    ->addIndex(
                        $installer->getIdxName('simpleplugz_locations', ['geo_location_lat']),
                        ['geo_location_lat']
                    )
                    ->addIndex(
                        $installer->getIdxName('simpleplugz_locations', ['geo_location_lng']),
                        ['geo_location_lng']
                    )
                    ->setComment('Locations');
                $installer->getConnection()->createTable($table);
        }

        //add  the store hours to the database
        if (!$installer->tableExists('simpleplugz_locations_hours')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('simpleplugz_locations_hours'));
            $table->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                'Id'
            )
                ->addColumn(
                    'location_id',
                    Table::TYPE_INTEGER,
                    255,
                    ['nullable'  => false],
                    'Location Id'
                )
                ->addColumn(
                    'day',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable'  => false],
                    'Day'
                )
                ->addColumn(
                    'special_date',
                    Table::TYPE_DATE,
                    null,
                    ['nullable'  => true],
                    'Special Date'
                )
                ->addColumn(
                    'start_time',
                    Table::TYPE_TEXT,
                    8,
                    ['nullable'  => true],
                    'Start Time'
                )
                ->addColumn(
                    'end_time',
                    Table::TYPE_TEXT,
                    8,
                    ['nullable'  => true],
                    'End Time'
                )
                ->addColumn(
                    'type',
                    Table::TYPE_TEXT,
                    20,
                    ['nullable'  => false],
                    'Type'
                )
                ->addColumn(
                    'closed',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable'  => false],
                    'Closed'
                )
                ->addColumn(
                    'active',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable'  => false],
                    'Active'
                )->addIndex(
                    $installer->getIdxName('simpleplugz_locations_hours', ['special_date']),
                    ['special_date']
                )
                ->setComment('Locations Hours');

                $installer->getConnection()->createTable($table);
        } //end check if the table is already in the database
        $installer->endSetup();
    }
}
