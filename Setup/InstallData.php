<?php namespace Simpleplugz\Locations\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    private $locate;
    private $directory_list;
    private $configWriter;
    private $logger;
    private $connection;
    private $stdClass;
    private $reader;
    private $locateFactory;
    private $hoursFactory;
    private $urlRewriteFactory;
    private $storeManager;
    private $file;

    public function __construct(
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Psr\Log\LoggerInterface $logger,
        \Simpleplugz\Locations\Model\SimpleplugzStdClass $stdClass,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Module\Dir\Reader $reader,
        \Simpleplugz\Locations\Model\LocationsItemFactory $locateFactory,
        \Simpleplugz\Locations\Model\LocationsHoursFactory $hoursFactory,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Io\File $file
    ) {
        $this->directory_list = $directory_list;
        $this->configWriter = $configWriter;
        $this->logger = $logger;
        $this->stdClass = $stdClass;
        $this->locateFactory = $locateFactory;
        $this->hoursFactory = $hoursFactory;
        $this->storeManager = $storeManager;
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->reader = $reader;
        $this->connection = $resource->getConnection();
        $this->file = $file;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $this->addDemoLocation();
        $this->addDemoWeeklyHours();
        $this->addDemoSpecial();
        $this->addDemoSEOUrl();
        $sampleFiles = $this->copySampleFiles();

        //load default configs
        $this->loadDefaultConfig($sampleFiles->usericon, $sampleFiles->storeIcon);
    }

    /**
     * Add Demo Location
     */
    public function addDemoLocation()
    {
        try {
            $data = [
                          'location_name' => 'New Store Example',
                          'location_address' => 'Avenue A & E 7th St, New York, NY 10014, USA',
                          'geo_location_lat' => '40.77435303',
                          'geo_location_lng' => '-73.95655823',
                          'custom_pointer' => 'stores/mapicon.png',
                          'details_texts' => '<h2>About this store</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam eos molestias modi, repellat sit, hic optio explicabo eligendi quisquam quibusdam veritatis. Minima aspernatur itaque aperiam eos quam, ad culpa placeat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam eos molestias modi, repellat sit, hic optio explicabo eligendi quisquam quibusdam veritatis. Minima aspernatur itaque aperiam eos quam, ad culpa placeat.</p>
    <h2>Parking is easy</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam eos molestias modi, repellat sit, hic optio explicabo eligendi quisquam quibusdam veritatis. Minima aspernatur itaque aperiam eos quam, ad culpa placeat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam eos molestias modi, repellat sit, hic optio explicabo eligendi quisquam quibusdam veritatis. Minima aspernatur itaque aperiam eos quam, ad culpa placeat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam eos molestias modi, repellat sit, hic optio explicabo eligendi quisquam quibusdam veritatis. Minima aspernatur itaque aperiam eos quam, ad culpa placeat.</p>',
                          'show_details_page' => '1',
                          'page_url' => 'store-example',
                          'show_closing_times' => '1',
                          'phone' => '0788888888',
                          'email' => 'example@email.com',
                          'image' => 'stores/storefront.jpg',
                          'show_manager' => '0',
                          'manager_image' => 'stores/manager.png',
                          'manager_name' => 'Sir Manages A. Lot',
                          'manager_email' => 'example@email.com',
                          'manager_phone' => '0788888888',
                          'active' => '1',
                          'default' => '1'];

            $this->locateFactory->create()->setData($data)->save();
        } catch (\Magento\Framework\Model\Exception $e) {
            throw new \Exception('Could not add Demo Location');
            //record the error
            $this->logger->debug('Could not add Demo Location');
        }
    }

    public function addDemoWeeklyHours()
    {
        try {
            $days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
            $collection = $this->hoursFactory->create()->getCollection();
            foreach ($days as $day) {
                $data = [
                        'location_id' =>1,
                        'day' => $day,
                        'special_date' => null,
                        'start_time' => '09:00:00',
                        'end_time' =>'17:00:00',
                        'type' => 'day',
                        'closed' => '0',
                        'active' => '1'
                          ];
                    $dataObject = $this->hoursFactory->create()->setData($data);
                    $collection->addItem($dataObject);
            }
            $collection->save();
        } catch (\Magento\Framework\Model\Exception $e) {
            throw new \Exception('Could not add Demo Weekly Hours');
            //record the error
            $this->logger->debug('Could not add Demo Weekly Hours');
        }
    }

    public function addDemoSpecial()
    {
        try {
            $data = [
                    'location_id' =>1,
                    'day' => 'Christmas',
                    'special_date' => date('Y', strtotime('now')).'-12-25',
                    'start_time' => '09:00:00',
                    'end_time' =>'17:00:00',
                    'type' => 'special',
                    'closed' => '1',
                    'active' => '1'
                      ];
            $this->hoursFactory->create()->setData($data)->save();
        } catch (\Magento\Framework\Model\Exception $e) {
            throw new \Exception('Could not addDemoSpecial');
            //record the error
            $this->logger->debug('Could not addDemoSpecial');
        }
    }

    private function copySampleFiles()
    {
        $storesFolderLocation = $this->directory_list->getPath('media').'/stores';
        try {
            //make a directoty to put the samplefiles in
            // if (!is_dir($storesFolderLocation)) {
            //     mkdir($storesFolderLocation, 0755, true);
            // }

            if (!is_dir($storesFolderLocation)) {
                $ioAdapter = $this->file;
                $ioAdapter->mkdir($storesFolderLocation, 0775);
            }

             $demoFolderLocation = $this->df_module_dir();
            /**
             * copy the mapicon
            */
            $copier = $this->file;
            $copier->cp($demoFolderLocation.'/demo/Images/manager.png', $storesFolderLocation.'/manager.png');
            $copier->cp($demoFolderLocation.'/demo/Images/mapicon.png', $storesFolderLocation.'/mapicon.png');
            $copier->cp($demoFolderLocation.'/demo/Images/usericon.png', $storesFolderLocation.'/usericon.png');
            $copier->cp($demoFolderLocation.'/demo/Images/storefront.jpg', $storesFolderLocation.'/storefront.jpg');

            $sampleFiles = $this->stdClass->create();
            $sampleFiles->storeIcon = 'mapicon.png';
            $sampleFiles->usericon = 'usericon.png';
            $sampleFiles->managerImage = 'stores/manager.png';
            $sampleFiles->storeImage = 'stores/storefront.jpg';
            return $sampleFiles;
        } catch (\Magento\Framework\Model\Exception $e) {
            throw new \Exception('Could not copy sample files');
            //record the error
            $this->logger->debug('Could not copy sample files');
        }
    }

    //get location of Simpleplugz/Locations/
    private function df_module_dir()
    {
        $moduleName = 'Simpleplugz_Locations';
        return $this->reader->getModuleDir('', $moduleName);
    }

    /**
     * Add Demo seo url so it works
     */
    private function addDemoSEOUrl()
    {
        try {
            $url = 'store-example';
            $urlRewriteModel = $this->urlRewriteFactory->create();
            /* set current store id */
            $urlRewriteModel->setStoreId($this->storeManager->getStore()->getId()); // this magneot store id not location id
            /* this url is not created by system so set as 0 */
            $urlRewriteModel->setIsSystem(0);
            /* unique identifier - set random unique value to id path */
            $urlRewriteModel->setIdPath(rand(1, 100000));
            /* set actual url path to target path field */
            $urlRewriteModel->setTargetPath("locations/store/index/storeid/1");
            /* set requested path which you want to create */
            $urlRewriteModel->setRequestPath($url);
            /* set current store id */
            $urlRewriteModel->save();
        } catch (\Magento\Framework\Model\Exception $e) {
            //record the error
            $this->logger->debug('Path already Exists - locations/store/index/storeid/1');
        }
    }

    //load a default config information
    private function loadDefaultConfig($userIcon, $storeIcon)
    {
        //set the default countries to be blank so it selects all
        $this->configWriter->save('store_locations/store_locations_group/google_api_code', '');

        //set the default countries to be blank so it selects all
        $this->configWriter->save('store_locations/store_locations_group/countries', '');

        //allow geo detaction
        $this->configWriter->save('store_locations/store_locations_group/allow_geo_detect', '1');

        //default icon
        $this->configWriter->save('store_locations/store_locations_group/default_icon', $storeIcon);

        //set user icon
        $this->configWriter->save('store_locations/store_locations_group/user_icon', $userIcon);

        //distance type KM or MILES
        $this->configWriter->save('store_locations/store_locations_group/distance_type', 'miles');

        //set geo search_radius
        $this->configWriter->save('store_locations/store_locations_group/search_radius', '');
    }
}
