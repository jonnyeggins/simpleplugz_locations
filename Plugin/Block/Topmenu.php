<?php
/**
 * Simpleplugz_Locations extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Simpleplugz
 * @package   Simpleplugz_Locations
 * @copyright 2016 Jonny Eggins
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @author    Jonny Eggins
 */
namespace Simpleplugz\Locations\Plugin\Block;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Data\Tree\Node;
use Magento\Theme\Block\Html\Topmenu as TopmenuBlock;

class Topmenu
{
    /**
     * @var Url
     */
    protected $url;
    /**
     * @var Http
     */
    protected $request;

    /**
     * @param Url  $url
     * @param Http $request
     */
    public function __construct(
        Http $request
    ) {
        $this->request  = $request;
    }

    /**
     * @param TopmenuBlock $subject
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @param int $limit
     * @SuppressWarnings("PMD.UnusedFormalParameter")
     */
    // @codingStandardsIgnoreStart
    public function beforeGetHtml(
        TopmenuBlock $subject,
        $outermostClass = '',
        $childrenWrapClass = '',
        $limit = 0
    ) {
        // @codingStandardsIgnoreEnd
        $node = new Node(
            $this->getNodeAsArray(),
            'id',
            $subject->getMenu()->getTree(),
            $subject->getMenu()
        );
        $subject->getMenu()->addChild($node);
    }

    /**
     * @return array
     */
    protected function getNodeAsArray()
    {
        return [
            'name' => __('Store Finder'),
            'id' => 'store-finder',
            'url' => '/locations',
            'has_active' => false,
            'is_active' => false
        ];
    }
}
