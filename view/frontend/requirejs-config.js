var randomnumber = Math.random();
var config = {
    // urlArgs: "bust=",
    // urlArgs: "bust=" + (new Date()).getTime(),
    paths:{
        "async":"Simpleplugz_Locations/js/lib/async",
        "lodash":"Simpleplugz_Locations/js/lib/lodash",
        "storeDetailsjs":"Simpleplugz_Locations/js/fullStoreDetails",
        "mapjs":"Simpleplugz_Locations/js/map",
        "locations":"Simpleplugz_Locations/js/locations",
        "locationsTheme":"Simpleplugz_Locations/js/locationsTheme",
        "modal":'Magento_Ui/js/modal/modal'
    },
    shim: {
        'locations': {
            deps: ['jquery']
        },
        'storeDetailsjs': {
            deps: ['jquery']
        },
        'mapjs': {
            deps: ['jquery','locations']
        },
        'locationsTheme': {
            deps: ['jquery','locations']
        },
        'storeAdminjs': {
            deps: ['jquery']
        }
    }

};


