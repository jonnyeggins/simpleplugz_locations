
//get Popup Template
function getPopupTemplate(store){
    var locationPlus = store.location_address;
    locationPlus = locationPlus;
    var start = jQuery('#user_address').val();

    store.directions = 'https://maps.google.com?saddr='+start+'&daddr='+locationPlus;

    var returned = "<div><div class=\"topheader\"><span>"+store.location_name+"</span></div> \
        <div class=\"mappopupinfo row \"> \
            <div class=\"col-sm-6\"> \
            <label>Address:</label> \
            <br><span>" + store.location_address + "</span> \
            <div class=\"spacer\"></div>";
    if (store.phone !== '' && store.phone != null) {
        returned =  returned + "<label>Phone: </label><span>" + store.phone + "</span>";
    }
    returned =  returned + "\
            </div> \
            <div class=\"col-sm-6\"> \
                <div class=\"streetview\" style=\"float:right\"> ";
    if (store.image != '' && store.image != null) {
        returned =  returned + "<img class=\"smallstore\" src=\""+store.image+"\" alt=\"smallstoreimage\">";
    }
    if (store.show_details_page == 1){
        var fullstoredetails = "<a href=\""+store.fulldetails+"\" target=\"_blank\" class=\"GetFullStoreDetails\">More details ➔</a>";
    } else {
        var fullstoredetails = "";
    }
    returned =  returned + "</div>   \
            </div> \
            </div> \
            <div class=\"mappopupinfo row bottombox \">\
            <div class=\"col-sm-6 col-xs-6\">\
            " + fullstoredetails + "\
            </div>\
            <div class=\"col-sm-6 col-xs-6 getdirections\">\
            <a href=\""+store.directions+"\" class=\"getDirections\" target=\"_blank\">Get Directions ➔</a>\
            </div>\
             </div>\
            </div>";
    return returned;
}

//this just does a check to see oif they are a mobile user yes or no
function detectMobileUser(){
    var width = jQuery(window).width(), height = jQuery(window).height();
    if (width <= 480) {
        settings.mobile = true;

        //remove the class
        jQuery('#storefinder').removeClass('deskview');
        jQuery('#storefinder').addClass('mobileview');
    } else {
        settings.mobile = false;
        jQuery('#storefinder').addClass('deskview');
        jQuery('#storefinder').removeClass('mobileview');
    }
}

//change the mobi view
function checkMobiButton(selected){
    if (settings.splitmobile == 'fullwidth'){
        jQuery('#mapview').addClass('active');
        jQuery('#listview').addClass('active');
        jQuery('#mobiview_list').addClass('active');
        jQuery('#mobiview_map').addClass('active');
    } else {
        if (selected == 'map') {
             jQuery('.maparea').removeClass('mobilist');
             jQuery('.maparea').addClass('mobimap');
             jQuery('#listview').removeClass('active');
             jQuery('#mapview').addClass('active');
             jQuery('#mobiview_map').addClass('active');
             jQuery('#mobiview_list').removeClass('active');
        }

        if (selected == 'list') {
             jQuery('#mapview').removeClass('active');
             jQuery('#listview').addClass('active');
             jQuery('#mobiview_list').addClass('active');
             jQuery('#mobiview_map').removeClass('active');
             jQuery('.maparea').removeClass('mobimap');
             jQuery('.maparea').addClass('mobilist');
        }
    }

    //update the settings
    settings.mobileview = selected;
}

// //show the lable of current location
// function showCurrentLocation(){
//  jQuery('#current_location').show();
//  jQuery('#emptysearch').hide();
// }

// //hide the lable of current location
// function hideCurrentLocation(){
//  jQuery('#current_location').hide();
//  jQuery('#emptysearch').show();
// }    

function adjustViewBasedOnWidth(){
    if (jQuery('.maparea').width() < 960) {
            //area is too small need to move to full
           jQuery('#listview').addClass('col-md-12');
           jQuery('#listview').addClass('fullview');
           jQuery('#mapview').addClass('col-md-12');
           jQuery('#mapview').addClass('fullview');
           jQuery('#listview').removeClass('col-md-5');
           jQuery('#mapview').removeClass('col-md-7');
           jQuery('#listview').removeClass('splitview');
           jQuery('#mapview').removeClass('splitview');
           //adjust the screen slide to be half map and half info
            var screenheight = jQuery(window).height();
            jQuery('#map').css('min-height',(screenheight/1.3)+'px');
            
    } else {
           jQuery('#listview').addClass('col-md-5');
           jQuery('#mapview').addClass('col-md-7');
           jQuery('#listview').removeClass('col-md-12');
           jQuery('#mapview').removeClass('col-md-12');

           jQuery('#listview').addClass('splitview');
           jQuery('#mapview').addClass('splitview');
           jQuery('#listview').removeClass('fullview');
           jQuery('#mapview').removeClass('fullview');
    }


}

// function getSettings(){
jQuery(document).ready(function(){
     var screenheight = jQuery(window).height();
        jQuery('#map').css('min-height',(screenheight/1.3)+'px');
        
        adjustViewBasedOnWidth();
      jQuery( window ).on('resize',function(){ 
        adjustViewBasedOnWidth();
            
    });
    });