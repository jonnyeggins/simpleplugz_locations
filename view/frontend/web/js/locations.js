
//global varriables
//fallback settings
var settings = {};
//search type
var searchtype = 'postcode';
var map = null;
var markers = [];
var markersStoreInfo = [];
var userMarker = null;
var allLocations = [];
var autocomplete = '';
var autocompleteLsr = '';
var googlecheckcount = 0;
var infoWindow = false;
var directionsService = {};
var directionsDisplay = {};
var saved_user_lat = '';
var saved_user_ng = '';

//get settings from database
// function getSettings(){
jQuery(
    function(){
        //displsy ythe map size
       
        //post to the database

        jQuery('#user_address').on(
            'keyup',function(e){

                detectEmptyAddressSearch();
                if (e.keyCode == 13) {
                    var address = jQuery(".pac-container .pac-item:first").text();
                    checkAddress(address);
                }
            }
        );
        jQuery('#user_address').on(
            'blur',function(){
                jQuery('#nosearchfound').hide();
            }
        );

    
        
    }
);

//this script is loaded on a callback from loading the google maps api files
function googleMapsLoaded(){
    
    jQuery.post("/locations/index/getSettingsFromDatabase",{}, function( data ) {
            // settings = jQuery.parseJSON(data);
            settings = data;

            if (settings.splitmobile == 'split'){
                detectMobileUser();
            }

            //load the autocomplete for the postcode
            autoCompletePostcode();

            //once loaded settings
            loadMap();

            //detect user location
            if (settings.detectUser == true) {
                detectUsersLocation();
            } else {
                //load default list
                getDefaultLocations();
            }

        }
    );
}

function checkAddress(address){
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode(
        {"address": address}, function(results, status) {
            if (results[0] != undefined) {
                //address found
                var latvalue = results[0].geometry.location.lat();
                var lngvalue = results[0].geometry.location.lng();
                jQuery('#user_address').val(results[0].formatted_address);
                jQuery('#nosearchfound').hide();
                setUserLocationMarker(latvalue,lngvalue,'User Location');
                getClosestLocations(latvalue,lngvalue); //load all the store when it is finished
            } else {
                jQuery('#nosearchfound').show();
            }
        }
    );
}




//detect users
function detectUsersLocation(){
    //load the default positions then ask for their location
    getDefaultLocations();

    //check to make sure it only tries it on a htts or it wont be allowed
    if (location.protocol === 'https:') {
        //detect users locations

        //check to make sure the page is https
        if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        }

                        //show current location tag box
                        jQuery('#current_location').show();
                        setUserLocationMarker(geolocation.lat,geolocation.lng,'User Location');
                        // moveToLocation(geolocation.lat,geolocation.lng);

                        getClosestLocations(geolocation.lat,geolocation.lng);


                    }
                );
        } else {
            //load the default locations
            //getDefaultLocations();
        }
    } else {
        //load the default locations cause can detect the user lcoation
        //getDefaultLocations();
    }
}
function moveToUser(){
    if (saved_user_lat != undefined && saved_user_lat != ''){
        moveToLocation(saved_user_lat,saved_user_lng);
    }
}
//put the users location on the map
function setUserLocationMarker(lat,lng,address){

    saved_user_lat = lat;
    saved_user_lng = lng;
    if (userMarker != null) {
        removeUserMarker();
    }
    var position = new google.maps.LatLng(lat,lng);
    var markerOptions = {
        position: position,
        map: map,
        title: address,
        zoom:20,
        icon: settings.usericon
    };

    //save the google markers in an array to keep track
    userMarker = new google.maps.Marker(markerOptions);
}

//remove the users location //for when they change the lcocation
function removeUserMarker(){
    //remove userMarker;
    userMarker.setMap(null);
}

//autocomplete address
function autoCompleteAddress(){
    google.maps.event.removeListener(autocompleteLsr);
    google.maps.event.clearInstanceListeners(autocomplete);
    jQuery(".pac-container").remove();
    
    if (settings.countryDetect == '') {
        autocomplete = new google.maps.places.Autocomplete(
            /**
            * @type {!HTMLInputElement} 
            */ (
                    document.getElementById('user_address')), {
                  
               }
        );
    } else {
        autocomplete = new google.maps.places.Autocomplete(
            /**
            * @type {!HTMLInputElement} 
            */ (
                    document.getElementById('user_address')), {
                        // types: ['(regions)'],
                        componentRestrictions: {country:settings.countryDetect}
               }
        );
    }
          
    autocompleteLsr = autocomplete.addListener(
        'place_changed', function(){
            autoCompleteSelected(autocomplete);
        }
    );
    

}

//autocomplate postcode
function autoCompletePostcode(){
    
    google.maps.event.removeListener(autocompleteLsr);
    google.maps.event.clearInstanceListeners(autocomplete);
    jQuery(".pac-container").remove();
    jQuery('#user_address').on(
        'change',function(){
            if (jQuery('#user_address').val() != '') {
                jQuery('#emptysearch').show();
                jQuery('#current_location').hide();
                jQuery('#user_address').off('change');
            }
        }
    );
    
    if (settings.countryDetect == '') {
        autocomplete = new google.maps.places.Autocomplete(
            /**
            * @type {!HTMLInputElement} 
            */ (
                    document.getElementById('user_address')), {
                        types: ['(regions)']
                 }
        );
    } else {
        autocomplete = new google.maps.places.Autocomplete(
            /**
            * @type {!HTMLInputElement} 
            */ (
                    document.getElementById('user_address')), {
                        types: ['(regions)'],
                        componentRestrictions: {country:settings.countryDetect}
                 }
        );
    }
          
    autocompleteLsr = autocomplete.addListener(
        'place_changed', function(){
            autoCompleteSelected(autocomplete);
        }
    );


    
}


function autoCompleteUpdate(){
    //remove the old user
    removeUserMarker();

    //searchLocation
    searchLocation();

}

function emptySearch(){
    jQuery('#nosearchfound').hide();
    jQuery('#emptysearch').hide();
    jQuery('#user_address').val('');
    removeUserMarker();
    jQuery('#user_address').focus();
}

function detectEmptyAddressSearch(){
    
    if (jQuery('#user_address').val() == '') {
        jQuery('#nosearchfound').hide();
    } else {
        jQuery('#nosearchfound').show();
    }
    
}

function autoCompleteSelected(autocomplete){
    jQuery('#nosearchfound').hide();
    jQuery('#emptysearch').show();
    var place = autocomplete.getPlace();
    if (place.geometry == undefined) {
        jQuery('#nosearchfound').show();
        return false;
    }
    var user_lat = place.geometry.location.lat();
    var user_lng = place.geometry.location.lng();
    var searched_formatted_address = place.formatted_address;
    setUserLocationMarker(user_lat,user_lng,'User Location');
    // moveToLocation(user_lat,user_lng);
    
    getClosestLocations(user_lat,user_lng); //load all the store when it is finished
    
}

function detectClosestLocation(user_lat,user_lng){
    if (jQuery('#listview .location_info').length != 0) {
        var lat = jQuery(jQuery('#listview .location_info')[0]).data('lat');
        var lng = jQuery(jQuery('#listview .location_info')[0]).data('lng');
    }
    calculateAndDisplayRoute(user_lat+','+user_lng,lat+','+lng);
}

function moveToLocation(lat,lng){
    var newCenter = { 
        'lat': lat,
        'lng': lng
    };

    map.panTo(newCenter);
}

//get the closest locations
function getClosestLocations(lat,lng){
    if (jQuery('#distanceToFar').length >= 1){
        jQuery('#distanceToFar').hide();
    }
    jQuery('#listview').empty().append('<div class="spinner"></div>');
    var address = jQuery('#user_address').val();
    jQuery.post(
        "/locations/index/getLocationsFromGeo",{'lat':lat,'lng':lng}, function( data ) {
            jQuery('#listview').empty().append(data);
            detectClosestLocation(lat,lng);
        }
    );


}

function storeRowClicked(storeid){

    getDirectionsToStoreId(storeid);
    jQuery('.location_info').removeClass('active');
    jQuery('.location_info[data-storeid="'+storeid.toString()+'"]').addClass('active');
    //check to see if on the mobile list page
    if (jQuery('#listview.active').length >= 1) {
        //send them to the map and it should bring up the correct loction
        checkMobiButton('map');
    }
    loadMarkerPopup(storeid);
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    if (width <= 480) {

            //scroll to location on ap
        jQuery("body, html").animate({ 
          scrollTop: jQuery('#map').offset().top 
        }, 600);
            
    }
    // moveToLocation(store.)
}

function getDirectionsToStoreId(storeid){

    //check if user details have not been set yet
    if (saved_user_lat == '' || saved_user_lng == '' ) {

        return true;
    }
    var store_lat = jQuery('#listview .location_info[data-storeid="'+storeid+'"]').data('lat');
    var store_lng = jQuery('#listview .location_info[data-storeid="'+storeid+'"]').data('lng');

    calculateAndDisplayRoute(saved_user_lat+','+saved_user_lng,store_lat+','+store_lng);
}
//get Default Locations (displayed when the page is first loadeded)
function getDefaultLocations(){
    jQuery('#current_location').hide();
    //posting no info will just load the default locations
    jQuery('#listview').empty().append('<div class="spinner"></div>');
    var address = jQuery('#user_address').val();
    jQuery.post(
        "/locations/index/getLocationsFromGeo",{}, function( data ) {
            jQuery('#listview').empty().append(data);
        }
    );
}

//Get all Locations
function getAllLocations(){
    jQuery.post(
        "/locations/index/getAllLocations",{}, function( data ) {
            //set markers 
            // allLocations = jQuery.parseJSON(data);
            jQuery.each(
                data, function( index, store ){
                    //set each marker on the map
                    setMarker(store);
                }
            );
        }
    );
}

//load map
function loadMap(){

    //load the default store
    var mapOptions = {
        center: new google.maps.LatLng(settings.defaultLocation.lat, settings.defaultLocation.lng),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    //Setup directs api
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;

    if (map == null) {
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
    }
    directionsDisplay.setMap(map);
    directionsDisplay.setOptions({ suppressMarkers: true });

    if (settings.loadAll) { // wait till mapa is fully loaded before trying to put things on it
        google.maps.event.addListenerOnce(
            map, 'idle', function(){

                //load all the locations
                getAllLocations();
            }
        );
    }

}

function calculateAndDisplayRoute(origin,destination) {

    directionsDisplay.set('directions', null);
      directionsService.route(
          {
                origin: origin,
                destination: destination,
                travelMode: 'DRIVING'
           }, function(response, status) {
            if (status === 'OK') {
                jQuery('#distanceToFar').hide();
                directionsDisplay.setDirections(response);
            } else {
                if (status == 'ZERO_RESULTS'){
                    moveToUser();
                    jQuery('#distanceToFar').show();
                }
                //if it cant find it down try to do it
                //window.alert('Directions request failed due to ' + status);
            }
           }
      );
}

//Set Markers
function setMarker(store){
    if (store.custom_pointer == '' || store.custom_pointer == null) {
        var icon_image = settings.icon
    } else {
        var icon_image = store.custom_pointer;
    }
    var position = new google.maps.LatLng(parseFloat(store.geo_location_lat),parseFloat(store.geo_location_lng));
    var markerOptions = {
        position: position,
        map: map,
        title: store.location_name,
        zoom:20,
        icon: icon_image
    };
        //save the google mareks in an array to keep track
    markers[store.id] = new google.maps.Marker(markerOptions);

    //save the markers store info in var
    markersStoreInfo[store.id] = store;
        
    google.maps.event.addListener(
        markers[store.id], 'click', function (e) {
            //add a popup for each marker
            getDirectionsToStoreId(store.id);
            loadMarkerPopup(store.id);
        }
    );
}




//load the popup of the location
function loadMarkerPopup(storeid){
    var marker = markers[storeid];
    var store =  markersStoreInfo[storeid];
    var infoWindowOptions = {
        'content':getPopupTemplate(store),
            // 'maxWidth':'70%'
    };
    if (infoWindow) {
        infoWindow.close();
    }

        infoWindow = new google.maps.InfoWindow(infoWindowOptions);
        setTimeout(
            function(){ // set a delay so it displays after the directions
                infoWindow.open(map, marker);
            },200
        );

}



function setPostcodeSearch(){
    //setback to blank
    jQuery('#user_address').val('');
    //change the placeholder
    jQuery('#user_address').attr('placeholder','Enter Postcode');
    
    //add class active to tab class
    jQuery('.tabaddress').removeClass('active');
    
    //remove class from postcode	
    jQuery('.tabpostcode').addClass('active');
    
    //focus on the input box, so they can start typing
    jQuery('#user_address').focus();

    //set searchtype varraible as postcode
    
    searchtype = 'Postcode';
    
    //update autosearch
    autoCompletePostcode();
}


function setAddressSearch(){
    //setback to blank
    jQuery('#user_address').val('');

    //change the placeholder
    jQuery('#user_address').attr('placeholder','Full Address');
    
    //add class active to tab class
    jQuery('.tabaddress').addClass('active');
    
    //remove class from postcode	
    jQuery('.tabpostcode').removeClass('active');
    
    //focus on the input box, so they can start typing
    jQuery('#user_address').focus();
    
    //set searchtype varraible as address search
    searchtype = 'Fulladdress';
    
    //update autosearch
    autoCompleteAddress();
}

function clearSearch(){
    //set search to null
    jQuery('user_address').val('');
}

//search locations
function searchLocation(){
    getClosestLocations();
}

