//declare store settings variable
var storeSettings = {};
//declare map global variable
var map;

//called when google maps code has been loaded
function googleMapsLoaded(){
    getSettings();
}


//something is broken throw error message or redirects
function errorLoadingMap(){
    //redirect to to the store finder map
    window.location = "/locations";
}

//get the settings for displaying this store location
function getSettings(){

    var storeid = jQuery('#hiddenStoreId').val();

    jQuery.post(
        "/locations/index/getStoreDetailsSettings",{'storeid':storeid}, function( data ) {
            storeSettings = data;
            if (storeSettings != undefined && storeSettings.unknown != undefined) {
                errorLoadingMap();
            }
            // storeSettings = JSON.parse(data);

            loadMap(storeSettings.geo_location_lat,storeSettings.geo_location_lng);
        }
    );
}

//load the google map
function loadMap(lat,lng){

    //load the default store
    var mapOptions = {
        center: new google.maps.LatLng({lat: parseFloat(lat), lng: parseFloat(lng)}),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


                map = new google.maps.Map(document.getElementById("map"), mapOptions);

        var marker = new google.maps.Marker(
            {
                position: {lat: parseFloat(lat), lng: parseFloat(lng)},
                map: map,
                icon: storeSettings.icon,
                title: storeSettings.storename
            }
        );

}



