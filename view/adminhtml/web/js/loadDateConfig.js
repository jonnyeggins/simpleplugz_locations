define(['jquery'], function ($) {
    var dateJsComponent = function(settings)
    {
       // get thecorrect dateformat
        var dateformat = settings.dateFormat;
        //apply the date format to the datepicker
        $('.datepicker').datepicker({
            dateFormat:dateformat
        });
    };

    return dateJsComponent;
});