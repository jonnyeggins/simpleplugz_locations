
    var autocomplete;
    var googlecheckcount = 0;

    jQuery(document).on(
        "beforeSubmit", function(e) {
            if (!timesCheck()) {
                return false;
            }
            //format the weekly table data into a json and put into input box
            updateWeeklyTimes();
            //format the specials table data into a json and put into input box
            updateSpecialTimes();

            return true;
        }
    ); // end before submit

    //up the the hidden text box filed with a json format of the table
    function updateWeeklyTimes(){
        var times = {};
        times.sunday = [];
        times.monday = [];
        times.tuesday = [];
        times.wednesday = [];
        times.thursday = [];
        times.friday = [];
        times.saturday = [];
        jQuery('.weeklytimes tr ').each(
            function(){
                var day = {};

                day.open_h = jQuery(this).find('.storeopen').find('.renderTimesHours').val();
                day.open_m = jQuery(this).find('.storeopen').find('.renderTimesMins').val();
                day.open_ampm = jQuery(this).find('.storeopen').find('.renderTimesText').val();
                day.close_h = jQuery(this).find('.storeclose').find('.renderTimesHours').val();
                day.close_m = jQuery(this).find('.storeclose').find('.renderTimesMins').val();
                day.close_ampm = jQuery(this).find('.storeclose').find('.renderTimesText').val();
                day.closed = jQuery(this).find('.closing').val();

                if (jQuery(this).attr('data-day') == '0') {
                    times.sunday = day;
                }
                if (jQuery(this).attr('data-day') == '1') {
                    times.monday = day;
                }
                if (jQuery(this).attr('data-day') == '2') {
                    times.tuesday = day;
                }
                if (jQuery(this).attr('data-day') == '3') {
                    times.wednesday = day;
                }
                if (jQuery(this).attr('data-day') == '4') {
                    times.thursday = day;
                }
                if (jQuery(this).attr('data-day') == '5') {
                    times.friday = day;
                }
                if (jQuery(this).attr('data-day') == '6') {
                    times.saturday = day;
                }
            }
        );


        jQuery('#closing_times').val(JSON.stringify(times));

    }

    function addSpecialTimes(dateformat){
        var addNews = ' \
			<tr data-specialid=""><td><input type="text" class="datepicker" value=""></td><td><input type="text" value="" class="dayname"></td><td  class="storeopen"><select class="renderTimesHours select admin__control-select">\
	            <option data-hour="1">1</option>\
	            <option data-hour="2">2</option>\
	            <option data-hour="3">3</option>\
	            <option data-hour="4">4</option>\
	            <option data-hour="5">5</option>\
	            <option data-hour="6">6</option>\
	            <option data-hour="7">7</option>\
	            <option data-hour="8">8</option>\
	            <option data-hour="9" selected="">9</option>\
	            <option data-hour="10">10</option>\
	            <option data-hour="11">11</option>\
	            <option data-hour="12">12</option>\
	        </select><select class="renderTimesMins select admin__control-select">\
	            <option data-min="00">00</option>\
	            <option data-min="05">05</option>\
	            <option data-min="10">10</option>\
	            <option data-min="15">15</option>\
	            <option data-min="20">20</option>\
	            <option data-min="25">25</option>\
	            <option data-min="30">30</option>\
	            <option data-min="35">35</option>\
	            <option data-min="40">40</option>\
	            <option data-min="45">45</option>\
	            <option data-min="50">50</option>\
	            <option data-min="55">55</option>\
	        </select><select class="renderTimesText select admin__control-select">\
	            <option value="AM" selected>AM</option>\
	            <option value="PM">PM</option>\
	        </select></td><td  class="storeclose"><select class="renderTimesHours select admin__control-select">\
	            <option data-hour="1">1</option>\
	            <option data-hour="2">2</option>\
	            <option data-hour="3">3</option>\
	            <option data-hour="4">4</option>\
	            <option data-hour="5" selected="">5</option>\
	            <option data-hour="6">6</option>\
	            <option data-hour="7">7</option>\
	            <option data-hour="8">8</option>\
	            <option data-hour="9">9</option>\
	            <option data-hour="10">10</option>\
	            <option data-hour="11">11</option>\
	            <option data-hour="12">12</option>\
	        </select><select class="renderTimesMins select admin__control-select">\
	            <option data-min="00">00</option>\
	            <option data-min="05">05</option>\
	            <option data-min="10">10</option>\
	            <option data-min="15">15</option>\
	            <option data-min="20">20</option>\
	            <option data-min="25">25</option>\
	            <option data-min="30">30</option>\
	            <option data-min="35">35</option>\
	            <option data-min="40">40</option>\
	            <option data-min="45">45</option>\
	            <option data-min="50">50</option>\
	            <option data-min="55">55</option>\
	        </select><select class="renderTimesText select admin__control-select">\
	            <option value="AM">AM</option>\
	            <option value="PM" selected>PM</option>\
	        </select></td><td><input type="checkbox" class="closing" data-day="0" data-time="2"  onclick="closedCheckboxClick(jQuery(this));"> Closed</td>\
	        <td><a href="#" onclick="cancelNewLine(jQuery(this));return false;"> Cancel</a></td>\
	    </tr>';
        jQuery('.specialtimes tbody').append(addNews);
         jQuery('.datepicker').datepicker(
             {
                    // dateFormat:'dd/mm/yy'
                    dateFormat:dateformat
             }
         );
    }

    function removeClosingTimes($this){
        jQuery($this).parents('tr').remove();
    }
    //remove the line
    function cancelNewLine($this){
         jQuery($this).parents('tr').remove();
    }

    function closedCheckboxClick($this){
        if (jQuery($this).is(':checked') == true) {
            jQuery($this).parents('tr').find('.renderTimesHours').prop("disabled", true);
            jQuery($this).parents('tr').find('.renderTimesMins').prop("disabled", true);
            jQuery($this).parents('tr').find('.renderTimesText').prop("disabled", true);
        } else {
            jQuery($this).parent().parent().find('select').prop("disabled", false);
        }
    }



    //setup google maps autocomplete address
    function setupAutocomplete(){
        if (jQuery('#geo_location_lat').val() == '') {
            jQuery('#addressCorrect').hide();
            jQuery('#addressError').show();
        } else {
            jQuery('#addressCorrect').show();
            jQuery('#addressError').hide();
        }
              var input = document.getElementById('location_address');

               autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.addListener(
                'place_changed', function(){
                    updateGeoFromAutoComplete();
                            
                }
            );

        
         jQuery('#location_address').on(
             'blur',function(){
                checkAddress();
             }
         );

            

    }

    function checkAddress(){
        var address = jQuery('#location_address').val();
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode(
            {"address": address}, function(results, status) {
                if (results[0] != undefined) {
                    //address found
                    var latvalue = results[0].geometry.location.lat();
                    jQuery('#geo_location_lat').val(latvalue);
                    var lngvalue = results[0].geometry.location.lng();
                    jQuery('#geo_location_lng').val(lngvalue);
                    jQuery('#addressCorrect').show();
                    jQuery('#addressError').hide();
                } else {
                    //address not found
                    //set lattitude to ''
                    jQuery('#geo_location_lat').val('');
                    jQuery('#geo_location_lng').val('');
                    jQuery('#addressCorrect').hide();
                    jQuery('#addressError').show();
                }
            }
        );
    }


    function updateGeoFromAutoComplete(){
        var place = autocomplete.getPlace();
        if (place === undefined || place.geometry === undefined ||  place.geometry.location === undefined ||  place.geometry.location.lat() == '') {
            jQuery('#addressCorrect').hide();
            jQuery('#addressError').show();
            return false;
        }
        var latvalue = place.geometry.location.lat();
        jQuery('#geo_location_lat').val(latvalue);
        var lngvalue = place.geometry.location.lng();
        jQuery('#geo_location_lng').val(lngvalue);
        jQuery('#addressCorrect').show();
        jQuery('#addressError').hide();
        return true;
    
    }

    function validateSpecialTimes(){
        //check to make sure there is a name for the special day
        jQuery('.specialtimes tr .dayname').each(
            function(){

            }
        );
    }


    //chec kto make sure the open time is less thant the closing time
    function timesCheck(){
        var failedToPass = 0;
        jQuery('.timeerror').remove('timeerror');
        jQuery('.closingtimes tr').each(
            function(){
                //skipt the heading tr line
                if (jQuery(jQuery(this).find('.renderTimesHours')[0]).length == 0) {
                    return true;
                }

                // remove any existing errors
                var failed = 0;
                //check if the closed checkbnox is checked
                if (jQuery(this).find('.closing:checked').length != 0) {

                    //the start and end date is ignored anyway
                    return true;
                }

                var open_h = jQuery(jQuery(this).find('.renderTimesHours')[0]).val();
                var open_m = jQuery(jQuery(this).find('.renderTimesMins')[0]).val();
                var open_ampm = jQuery(jQuery(this).find('.renderTimesText')[0]).val();
                var close_h = jQuery(jQuery(this).find('.renderTimesHours')[1]).val();
                var close_m = jQuery(jQuery(this).find('.renderTimesMins')[1]).val();
                var close_ampm = jQuery(jQuery(this).find('.renderTimesText')[1]).val();

                //check if am and pm are totaly wrong
                if (open_ampm == 'AM' && close_ampm == 'PM') {

                    return true;
                } else if (open_ampm == 'PM' && close_ampm == 'AM') {

                    failed = 1;
                } else {

                    // continue on and check the hours
                }


                //check if closing hours is greater
                if (open_h < close_h) {

                    return true;
                } else if (open_h > close_h) { // check if the hours are greater

                    failed = 1;
                    //the closing hours are less then the opening hours and it doenst make sense
                } else {

                    //the hours are the same
                    //continue on an check the minutes
                } 

                //check if closing minutes is greater
                if (open_m < close_m) {
                    return true;
                } else if (open_m > close_m) { // check if the minutes are greater
                    failed = 1;
                    //the closing minutes are less then the opening minutes and it doenst make sense
                } else {
                    //the minutes are the same
                    //continue on an check the minutes which is weird
                    failed = 1;
                }

                //invalid time detected
                if (failed == 1) {
                    jQuery(this).find('.storeopen').append('<div style="color:red" class="timeerror">Open time must be less then closing time</div>');
                    jQuery(this).find('.storeclose').append('<div style="color:red" class="timeerror">Closing time must be less then opening time</div>');
                    failedToPass = 1;
                } else {
                    jQuery(this).find('.timeerror').hide();
                }

            }
        );
        if (failedToPass == 0) {
            return true;
        } else {
            return false;
        }
    }

    function updateSpecialTimes(){
        var specialTimes = [];
        jQuery('.specialtimes tbody tr').each(
            function(){
                var time = {};
                time.id = jQuery(this).attr('data-specialid');
                time.date = jQuery(this).find('.datepicker').val();
                time.dayname = jQuery(this).find('.dayname').val();

                time.open_h = jQuery(jQuery(this).find('.renderTimesHours')[0]).val();
                time.open_m = jQuery(jQuery(this).find('.renderTimesMins')[0]).val();
                time.open_ampm = jQuery(jQuery(this).find('.renderTimesText')[0]).val();
                // time.starttime = time.hours+':'+time.mins+''+time.ampm;

                time.close_h = jQuery(jQuery(this).find('.renderTimesHours')[1]).val();
                time.close_m = jQuery(jQuery(this).find('.renderTimesMins')[1]).val();
                time.close_ampm = jQuery(jQuery(this).find('.renderTimesText')[1]).val();
                if (jQuery(this).find('.closing:checked').length != 0) {
                    time.closed = 1;
                } else {
                    time.closed = 0;
                }
                // time.closed = jQuery(this).find('.closing').val();
                // time.endtime = time.hours+':'+time.mins+''+time.ampm;
                specialTimes.push(time);

            }
        );

        //save the speical times to a hidden textbox
        jQuery('#closing_times_special').val(JSON.stringify(specialTimes));

    }

    //auto fill in the page_url with a sugested url
    function updateSlug(){
        if (jQuery('#page_url').val() == ''){
            var slug = jQuery('#location_name').val()
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
            jQuery('#page_url').val(slug);
        }


    }

    //when the document is loaded
    jQuery(document).ready(
        function(){
             //show hide depending on cloing time
             jQuery('#show_closing_times1').on(
                 'change',function(){
                    if (jQuery('#show_closing_times1:checked').length != 0) {
                        jQuery('.field-closing_times_special').show();
                    } else {
                        jQuery('.field-closing_times_special').hide();
                    }
                 }
             );
            jQuery('#location_name').on('change',function(){
                updateSlug();
            });
            jQuery('#show_closing_times0').on(
                'change',function(){
                    if (jQuery('#show_closing_times1:checked').length != 0) {
                        jQuery('.field-closing_times_special').show();
                    } else {
                        jQuery('.field-closing_times_special').hide();
                    }
                }
            );

            if (jQuery('#show_closing_times1:checked').length != 0) {
                jQuery('.field-closing_times_special').show();
            } else {
                jQuery('.field-closing_times_special').hide();
            }


             //show hide depending on show manager
             jQuery('#show_manager1').on(
                 'change',function(){
                    if (jQuery('#show_manager1:checked').length != 0) {
                        jQuery('.field-manager_name').show();
                        jQuery('.field-manager_email').show();
                        jQuery('.field-manager_image').show();
                        jQuery('.field-manager_phone').show();
                    } else {
                        jQuery('.field-manager_name').hide();
                        jQuery('.field-manager_email').hide();
                        jQuery('.field-manager_image').hide();
                        jQuery('.field-manager_phone').hide();
                    }
                 }
             );

            jQuery('#show_manager0').on(
                'change',function(){
                    if (jQuery('#show_manager1:checked').length != 0) {
                        jQuery('.field-manager_name').show();
                        jQuery('.field-manager_email').show();
                        jQuery('.field-manager_image').show();
                        jQuery('.field-manager_phone').show();
                    } else {
                        jQuery('.field-manager_name').hide();
                        jQuery('.field-manager_email').hide();
                        jQuery('.field-manager_image').hide();
                        jQuery('.field-manager_phone').hide();
                    }
                }
            );

            if (jQuery('#show_manager1:checked').length != 0) {
                jQuery('.field-manager_name').show();
                jQuery('.field-manager_email').show();
                jQuery('.field-manager_image').show();
                jQuery('.field-manager_phone').show();
            } else {
                jQuery('.field-manager_name').hide();
                jQuery('.field-manager_email').hide();
                jQuery('.field-manager_image').hide();
                jQuery('.field-manager_phone').hide();
            }

             //show hide details page
             jQuery('#show_details_page1').on(
                 'change',function(){
                    if (jQuery('#show_details_page1:checked').length != 0) {
                        jQuery('.field-page_url').show();
                        jQuery('.field-phone').show();
                        jQuery('.field-email').show();
                        jQuery('.field-details_texts').show();
                        jQuery('#base_fieldset_times').show();
                        jQuery('#base_fieldset_manager').show();
                    } else {
                        jQuery('.field-page_url').hide();
                        jQuery('.field-phone').hide();
                        jQuery('.field-email').hide();
                        jQuery('.field-details_texts').hide();
                        jQuery('#base_fieldset_times').hide();
                        jQuery('#base_fieldset_manager').hide();

                    }
                 }
             );

            jQuery('#show_details_page0').on(
                'change',function(){
                    if (jQuery('#show_details_page1:checked').length != 0) {
                        jQuery('.field-page_url').show();
                        jQuery('.field-phone').show();
                        jQuery('.field-email').show();
                        jQuery('.field-details_texts').show();
                        jQuery('#base_fieldset_times').show();
                        jQuery('#base_fieldset_manager').show();
                    } else {
                        jQuery('.field-page_url').hide();
                        jQuery('.field-phone').hide();
                        jQuery('.field-email').hide();
                        jQuery('.field-details_texts').hide();
                        jQuery('#base_fieldset_times').hide();
                        jQuery('#base_fieldset_manager').hide();

                    }
                }
            );

            if (jQuery('#show_details_page1:checked').length != 0) {
                jQuery('.field-page_url').show();
                jQuery('.field-phone').show();
                jQuery('.field-email').show();
                jQuery('.field-details_texts').show();
                jQuery('#base_fieldset_times').show();
                jQuery('#base_fieldset_manager').show();
            } else {
                jQuery('.field-page_url').hide();
                jQuery('.field-phone').hide();
                jQuery('.field-email').hide();
                jQuery('.field-details_texts').hide();
                jQuery('#base_fieldset_times').hide();
                jQuery('#base_fieldset_manager').hide();

            }

                //hide hidde text areas that are not needed to be visialbe but he values nweed to be passed through
                jQuery('.field-geo_location_lat').hide();
                jQuery('.field-geo_location_lng').hide();
                jQuery('#closing_times').parents('.admin__field').hide();
                jQuery('#closing_times_special').hide();
              

        }
    ); //end jquery ready

    //load when the google maps code is loaded
    function googleMapsLoaded(){
        setupAutocomplete();
    }
