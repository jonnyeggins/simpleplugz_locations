define([
    'jquery'
], function ($) {
    'use strict';

    return function (config) {
        console.log(config); // will output {a: "Hello from template"}
        alert(config.a); // would be equal to alert("Hello from template");
    }
});