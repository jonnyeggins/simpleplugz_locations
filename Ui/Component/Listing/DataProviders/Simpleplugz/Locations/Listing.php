<?php
namespace Simpleplugz\Locations\Ui\Component\Listing\DataProviders\Simpleplugz\Locations;

class Listing extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Simpleplugz\Locations\Model\ResourceModel\LocationsItem\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
