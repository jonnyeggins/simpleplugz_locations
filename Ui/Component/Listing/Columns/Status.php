<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Simpleplugz\Locations\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Status extends \Magento\Ui\Component\Listing\Columns\Column
{
    private $locate;
    protected $_assetRepo;
    // const NAME = 'thumbnail';

    // const ALT_FIELD = 'name';

    /**
     * @param ContextInterface                    $context
     * @param UiComponentFactory                  $uiComponentFactory
     * @param \Simpleplugz\Locations\Helper\Image $imageHelper
     * @param \Magento\Framework\UrlInterface     $urlBuilder
     * @param array                               $components
     * @param array                               $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Simpleplugz\Locations\Model\Locate $locate,
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->locate = $locate;
        $this->_assetRepo = $assetRepo;
    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $settings = $this->locate->getSettings();
            foreach ($dataSource['data']['items'] as & $item) {
                if ($item['active'] == '1') {
                    $item['active'] = '<img src="'. $this->_assetRepo->getUrl('Simpleplugz_Locations::images/tick.png').'" alt="tick" height="15px"/>
                        <span style="color:green">Enabled<span>';
                } else {
                    $item['active'] = '<img src="'. $this->_assetRepo->getUrl('Simpleplugz_Locations::images/close-button.png')  .'" alt="cross" height="15px"/>
                         <span style="color:black">Disabled</span>';
                }
                if ($item['default'] == '1') {
                    $item['default'] = '<span style="font-size: 189x;">&#9734;</span> Position 1';
                } else {
                    $item['default'] = '';
                }
            }
        }

        return $dataSource;
    }

    /**
     * @param array $row
     *
     * @return null|string
     */
    protected function getAlt($row)
    {
        $altField = $this->getData('config/altField') ?: self::ALT_FIELD;
        return isset($row[$altField]) ? $row[$altField] : null;
    }
}
