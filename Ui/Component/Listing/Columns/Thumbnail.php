<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Simpleplugz\Locations\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Thumbnail extends \Magento\Ui\Component\Listing\Columns\Column
{
    private $locate;
    const NAME = 'thumbnail';

    const ALT_FIELD = 'name';

    /**
     * @param ContextInterface                    $context
     * @param UiComponentFactory                  $uiComponentFactory
     * @param \Simpleplugz\Locations\Helper\Image $imageHelper
     * @param \Magento\Framework\UrlInterface     $urlBuilder
     * @param array                               $components
     * @param array                               $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Simpleplugz\Locations\Model\Locate $locate,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->locate = $locate;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if ($item['custom_pointer'] != '') {
                    $path = '/pub/media/';
                    $item['custom_pointer'] = $path. $item['custom_pointer'];
                    $item[$fieldName . '_src'] = $item['custom_pointer'];
                    $item[$fieldName . '_alt'] =  $item['custom_pointer'];
                    $item[$fieldName . '_link'] =  $item['custom_pointer'];
                    $item[$fieldName . '_orig_src'] = $item['custom_pointer'];
                } else {
                    $path = '';
                    $settings = $this->locate->getSettings();

                    $item[$fieldName . '_src'] =  $path.$settings->icon;
                    $item[$fieldName . '_alt'] =   $path.$settings->icon;
                    $item[$fieldName . '_link'] =   $path.$settings->icon;
                    $item[$fieldName . '_orig_src'] =  $path.$settings->icon;
                }
            }
        }

        return $dataSource;
    }

    /**
     * @param array $row
     *
     * @return null|string
     */
    protected function getAlt($row)
    {
        $altField = $this->getData('config/altField') ?: self::ALT_FIELD;
        return isset($row[$altField]) ? $row[$altField] : null;
    }
}
